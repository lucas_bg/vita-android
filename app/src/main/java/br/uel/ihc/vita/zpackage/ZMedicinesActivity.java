package br.uel.ihc.vita.zpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineAdapter;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

public class ZMedicinesActivity extends AppCompatActivity {

    protected Realm realm_instance;

    private ArrayList<Medicine> med_list;
    private ListView listView;
    private MedicineAdapter adapter;

    private Button btn_del;
    private Button btn_back;
    private Button btn_save_med;
    private Button btn_update_page;
    private Button btn_his;

    private UserDAO dao;
    private User mUser;

    private MedicineDAO med_dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.z_activity_medicines);

        // Initialize Realm
        Realm.init(this);
        this.realm_instance = Realm.getDefaultInstance();

        // Get User data
        String mId = getIntent().getStringExtra("USER_ID");
        System.out.println("USER_ID: " + mId);
        dao = new UserDAO(this.realm_instance);
        mUser = dao.getById(mId);
        System.out.println("USER OBJECT: " + mUser);

        // Inform user data
        TextView tx_user_name = findViewById(R.id.user_name_med_screen);
        if(mUser != null){
            tx_user_name.setText( mUser.getName() );
        }

        // set buttons //////////////////////////////////////////////////////////
        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ZMainActivity.class);
                startActivity(intent);
            }
        });

        btn_his = findViewById(R.id.btn_his);
        btn_his.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ZHistoryActivity.class);
                intent.putExtra("USER_ID", mUser.getId());
                startActivity(intent);
            }
        });

        btn_del = findViewById(R.id.btn_del);
        btn_del.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = mUser.getName();
                if(dao.delete( mUser )){
                    CharSequence text = "O Usuário " + name + " foi removido com sucesso!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(getBaseContext(), text, duration);
                    toast.show();

                    Intent intent = new Intent(getBaseContext(), ZMainActivity.class);
                    startActivity(intent);
                }else{
                    System.out.println("Houve um problema na deleção do registro.");
                }
            }
        });

        //btn_up_page
        btn_update_page = findViewById(R.id.btn_up_page);
        btn_update_page.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ZUserUpdateActivity.class);
                intent.putExtra("USER_ID", mUser.getId());
                startActivity(intent);
            }
        });

        btn_save_med = findViewById(R.id.btn_save_med);
        btn_save_med.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText input_title = findViewById(R.id.med_title);
                EditText input_dos_type = findViewById(R.id.med_dos_type);
                EditText input_dos = findViewById(R.id.med_dos);

                Medicine med_insert = new Medicine();
                med_insert.setTitle( input_title.getText().toString() );
                med_insert.setDosage_type( input_dos_type.getText().toString() );
                med_insert.setDosage( Integer.parseInt( input_dos.getText().toString()) );

                med_dao = new MedicineDAO( realm_instance );
                if(med_dao.save(mUser, med_insert ) != null){
                    System.out.println("Inserido com sucesso!");
                    input_title.setText("");
                    input_dos_type.setText("");
                    input_dos.setText( String.valueOf( 0 ) );
                    findMedicines();
                }else{
                    System.out.println("Houve um erro na inserção. Por favor, tente novamente.");
                }
            }
        });

        findMedicines();
    }

    public void findMedicines(){

        med_dao = new MedicineDAO( this.realm_instance );
        med_list = med_dao.list( mUser );

        listView = findViewById(R.id.medListView);
        adapter = new MedicineAdapter(med_list, getApplicationContext());
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = med_list.get(position).getTitle();
                if(med_dao.delete( med_list.get(position) )){

                    med_list.remove( position );
                    adapter.notifyDataSetChanged();

                    CharSequence text = "O Remédio " + name + " foi removido com sucesso!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(view.getContext(), text, duration);
                    toast.show();
                }else{
                    System.out.println("Houve um problema na deleção do registro.");
                }
            }
        });
    }

}
