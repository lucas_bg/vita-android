package br.uel.ihc.vita.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Classe criada para gerar objetos do tipo User.
 * @author Yago Henrique Pereira
 * @since 13/11/2017
 * @version 1.0
 */

public class User extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;
    private String email;
    private RealmList<Medicine> medicines;

    /**
     * Construtores
     */
    public User(){}

    public User(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(String name) {
        this.name = name;
    }

    /**
     * Setters & Getters
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RealmList<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(RealmList<Medicine> medicines) {
        this.medicines = medicines;
    }
}
