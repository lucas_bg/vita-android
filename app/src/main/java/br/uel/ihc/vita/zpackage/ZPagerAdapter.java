package br.uel.ihc.vita.zpackage;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Camila Sonoda on 12/11/2017.
 */

public class ZPagerAdapter extends FragmentStatePagerAdapter {
    int mNoOfTabs;

    public ZPagerAdapter(FragmentManager fm, int n){
        super(fm);
        this.mNoOfTabs = n;
    }

    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                return new ZTab_Ativos();
            case 1:
                return new ZTab_Inativos();
            default:
                return null;
        }
    }

    @Override
    public int getCount(){
        return mNoOfTabs;
    }


}
