package br.uel.ihc.vita;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.xdty.preference.colorpicker.ColorPickerDialog;
import org.xdty.preference.colorpicker.ColorPickerSwatch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.alarmsystem.SetAlarmsForMedicineList;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmList;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class NewRegisterTwoActivity extends AppCompatActivity {

    EditText etReceiveNotification;
    EditText input_remedio_observacao;
    ImageButton buttonColor;
    private int mSelectedColor;
    Boolean isSelectedColor;

    HashMap hashMapData;
    ArrayList arrayListWeekdays;

    String REGISTRATION_DIALOG = "REGISTRATION_DIALOG";

    View titulo_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_register_two);

        isSelectedColor = false;

        ImageButton buttonBack = (ImageButton) findViewById(R.id.btn_voltar);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button buttonRegister = (Button)findViewById(R.id.btn_proximo);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelectedColor && etReceiveNotification.getText().toString().length() > 0) {
                    saveMedicine();
                } else {
                    Toast.makeText(NewRegisterTwoActivity.this, "Preencha os campos para cor e notificação.", Toast.LENGTH_LONG).show();

                }
            }
        });

        input_remedio_observacao = (EditText)findViewById(R.id.input_remedio_observacao);

        etReceiveNotification = (EditText)findViewById(R.id.et_remedio_notificacao);
        etReceiveNotification.setKeyListener(null);
        etReceiveNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList arrayList = new ArrayList();
                arrayList.add("Sim");
                arrayList.add("Não");

                Utils.buildList(NewRegisterTwoActivity.this, arrayList, R.string.receive_notification_question, etReceiveNotification);
            }
        });

        Bundle extras = getIntent().getExtras();

        hashMapData = new HashMap();
        hashMapData = (HashMap) extras.getSerializable("hash");
        arrayListWeekdays = new ArrayList();
        arrayListWeekdays = (ArrayList) extras.getSerializable("weekdays");

        buttonColor = (ImageButton)findViewById(R.id.ibtn_cor);
        buttonColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int[] mColors = getResources().getIntArray(R.array.default_rainbow);

                ColorPickerDialog dialog = ColorPickerDialog.newInstance(R.string.color_picker_default_title,
                        mColors,
                        mSelectedColor,
                        5, // Number of columns
                        ColorPickerDialog.SIZE_SMALL,
                        true // True or False to enable or disable the serpentine effect
                        //0, // stroke width
                        //Color.BLACK // stroke color
                );

                dialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int color) {
                        ((GradientDrawable)buttonColor.getBackground()).setColor(color);
                        mSelectedColor = color;
                        isSelectedColor = true;
                    }

                });
                dialog.show(getFragmentManager(), "color_dialog_test");
            }
        });

        titulo_view = findViewById(R.id.titulo_tela);
        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(NewRegisterTwoActivity.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(titulo_view,
                        "Nesta tela você terminará de preencher as informações " +
                                "sobre o novo remédio e ao final irá clicar no botão" +
                                "\"TERMINEI!\" para cadastrar. Depois aguarde a tela de " +
                                "confirmação de sucesso do cadastro.", Utils.continuar);

                sequence.start();
            }
        });
    }

    void saveMedicine () {
        Realm realm_instance = Realm.getDefaultInstance();
        UserDAO dao = new UserDAO( realm_instance );
        ArrayList list = dao.list();
        User user = (User) list.get(0);
        Medicine medicine = new Medicine();
        medicine.setActive(true);

        String dateStartStr = (String) hashMapData.get("startDate");
        String dateEndStr = (String) hashMapData.get("finalDate");

        //save dates
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dateStart = format.parse(dateStartStr);
            medicine.setDate_start(dateStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            Date dateEnd = format.parse(dateEndStr);
            medicine.setDate_end(dateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RealmList realmList = new RealmList();
        for (Object weekday : arrayListWeekdays) {
            String str = (String) weekday;
            realmList.add(str);
        }
        medicine.setDays_of_week(realmList);

        medicine.setDescription(input_remedio_observacao.getText().toString());
        medicine.setDosage(Integer.valueOf(String.valueOf(hashMapData.get("qtd"))));
        medicine.setDosage_type((String) hashMapData.get("measure"));
        medicine.setInterval(Integer.valueOf(String.valueOf(hashMapData.get("intervalTime"))));
        medicine.setStart_time(Integer.valueOf(String.valueOf(hashMapData.get("startTime"))));

        //Notification
        if (etReceiveNotification.getText().toString().equals("Sim"))
            medicine.setNotification(true);
        else
            medicine.setNotification(false);

        medicine.setTitle((String) hashMapData.get("name"));

        //Continuous
        if (hashMapData.get("isContinous").toString().equals("Sim"))
            medicine.setUninterrupted(true);
        else
            medicine.setUninterrupted(false);

        medicine.setColor(String.valueOf(mSelectedColor));

        MedicineDAO medicineDAO = new MedicineDAO(realm_instance);
        medicine.setDays_of_week(changeDaysOfWeekString(medicine.getDays_of_week()));
        if (medicine.isUninterrupted()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(medicine.getDate_start());
//            calendar.add(Calendar.YEAR, 10);
            calendar.add(Calendar.MONTH, 1);
            // TODO depois alterar para 5 anos e resolver o problema em background
            medicine.setDate_end(calendar.getTime());
        }
        Medicine savedMedicine = medicineDAO.save(user, medicine);

        if (savedMedicine.isNotification()) {
            List<Medicine> list1 = new ArrayList<>();
            list1.add(savedMedicine);
            SetAlarmsForMedicineList obj = new SetAlarmsForMedicineList(list1, this);
            obj.setAllAlarms();
        }

        buildAlert();

    }

    private  void  buildAlert () {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(NewRegisterTwoActivity.this);
        LayoutInflater inflater = NewRegisterTwoActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.tela_alert_um_botao, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        Button button = dialogView.findViewById(R.id.buttonOk);
        button.setText(getString(R.string.ok));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                final Intent intent = new Intent(NewRegisterTwoActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        TextView textViewMessage = (TextView)dialogView.findViewById(R.id.tvMessage);
        textViewMessage.setText(getString(R.string.succesful));
    }



    private RealmList<String> changeDaysOfWeekString(RealmList<String> list) {
        RealmList<String> result = new RealmList<>();
        Calendar c = Calendar.getInstance();
        for(String str : list){
            switch (str) {
                case "1":
                    c.set(Calendar.DAY_OF_WEEK, 1);
                    break;
                case "2":
                    c.set(Calendar.DAY_OF_WEEK, 2);
                    break;
                case "3":
                    c.set(Calendar.DAY_OF_WEEK, 3);
                    break;
                case "4":
                    c.set(Calendar.DAY_OF_WEEK, 4);
                    break;
                case "5":
                    c.set(Calendar.DAY_OF_WEEK, 5);
                    break;
                case "6":
                    c.set(Calendar.DAY_OF_WEEK, 6);
                    break;
                case "7":
                    c.set(Calendar.DAY_OF_WEEK, 7);
                    break;
            }
            result.add(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
        }
        return result;
    }

}
