package br.uel.ihc.vita.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import br.uel.ihc.vita.NewRegisterActivity;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.adapters.PagerAdapter;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;


public class FragmentNewMedicine extends Fragment {

    private FrameLayout fragmentContainer;
    FragmentPagerAdapter adapterViewPager;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private View tab1;
    private View tab2;
    private Button buttonRegister;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_medicine, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        fragmentContainer = view.findViewById(R.id.fragment_container_2);

        buttonRegister = view.findViewById(R.id.btn_cadastrar_novo);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewRegisterActivity.class);
                startActivity(intent);
            }
        });

        tabLayout = view.findViewById(R.id.tablayout);
        //Initializing the tablayout

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Ativos"));
        tabLayout.addTab(tabLayout.newTab().setText("Inativos"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.vpPager);
        tab1 = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0);
        tab2 = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(1);

        adapterViewPager = new PagerAdapter(getActivity().getSupportFragmentManager(), viewPager);

        viewPager.setAdapter(adapterViewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ImageButton btnn = view.findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity());

                sequence.setConfig(config);

                sequence.addSequenceItem(buttonRegister,
                        "Neste botão você pode adicionar novos remédios!", Utils.continuar);

                sequence.addSequenceItem(tab1,
                        "Nesta aba são mostrados os remédios " +
                                "que estão ativos, ou seja, " +
                                "aqueles que você toma atualmente.", Utils.continuar);

                sequence.addSequenceItem(tab2,
                        "Já nesta aba estão os remédios inativos," +
                                " ou seja, aqueles que você parou de tomar mas " +
                                "que podem voltar aos ativos futuramente.", Utils.continuar);

                sequence.start();
            }
        });
        return view;
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
            fadeIn.setDuration(1000);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
            fadeOut.setStartOffset(1000);
            fadeOut.setDuration(1000);
            fragmentContainer.startAnimation(fadeOut);
        }

    }
}