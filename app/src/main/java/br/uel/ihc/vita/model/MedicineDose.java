package br.uel.ihc.vita.model;

import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by lucas on 29/11/2017
 */

public class MedicineDose implements Comparable {

    private Medicine med;
    private int dose;
    private Date diaEHora;
    private Boolean tomou;

    public Boolean isTomou() {
        return tomou;
    }

    public void setTomou(Boolean tomou) {
        this.tomou = tomou;
    }

    public Medicine getMed() {
        return med;
    }

    public void setMed(Medicine med) {
        this.med = med;
    }

    public int getDose() {
        return dose;
    }

    public void setDose(int dose) {
        this.dose = dose;
    }

    public Date getDiaEHora() {
        return diaEHora;
    }

    public void setDiaEHora(Date diaEHora) {
        this.diaEHora = diaEHora;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (!(o instanceof MedicineDose))
            throw new ClassCastException("A MedicineDose object expected.");
        int other_dose = ((MedicineDose) o).getDose();
        return this.getDose() - other_dose;
    }

}
