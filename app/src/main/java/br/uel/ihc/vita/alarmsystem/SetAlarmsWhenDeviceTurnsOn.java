package br.uel.ihc.vita.alarmsystem;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.Medicine;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lucas on 28/11/2017
 */

public class SetAlarmsWhenDeviceTurnsOn extends BroadcastReceiver {

    // RESETTING THE ALARMS AFTER ANDROID HAS BEEN SHUT DOWN
    @Override
    public void onReceive(Context context, Intent i) {
        // Verifying the Intent Action
        if (i.getAction() != null) {
            if (i.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Realm.init(context);
                RealmConfiguration config = new RealmConfiguration.Builder()
                        .name("jt.realm")
                        .schemaVersion(1)
                        .deleteRealmIfMigrationNeeded() // todo remove for production
                        .build();
                Realm.getInstance(config);
                Realm.setDefaultConfiguration(config);
                // Verificando por usuário
                Utils.createNewUserIfNeeded(context);
                // Querying medicines from database
                Realm realm_instance = Realm.getDefaultInstance();
                MedicineDAO medicineDAO = new MedicineDAO(realm_instance);
                List<Medicine> medicineList = medicineDAO.list(Utils.getUser(context));
                // Setting alarms from medicineList
                SetAlarmsForMedicineList obj = new SetAlarmsForMedicineList(medicineList, context);
                obj.setAllAlarms();
            }
        }
    }

}
