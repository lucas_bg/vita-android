package br.uel.ihc.vita;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;

import java.util.ArrayList;
import java.util.List;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.alarmsystem.SetAlarmsForMedicineList;
import br.uel.ihc.vita.fragments.FragmentMedicines;
import br.uel.ihc.vita.fragments.FragmentSettings;
import br.uel.ihc.vita.fragments.FragmentNewMedicine;
import br.uel.ihc.vita.model.Medicine;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class HomeActivity extends AppCompatActivity {

    private Fragment currentFragment;
    private FragmentMedicines fragmentMedicines = new FragmentMedicines();
    private FragmentNewMedicine fragmentNewMedicine = new FragmentNewMedicine();
    private FragmentSettings fragmentSettings = new FragmentSettings();

    private BottomViewPagerAdapter bottomViewPagerAdapter;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    // UI
    private AHBottomNavigationViewPager viewPagerBottom;
    private AHBottomNavigation bottomNavigation;

    protected Realm realm_instance;

    @Override
    protected void onRestart() {
        super.onRestart();

        getSupportFragmentManager()
                .beginTransaction()
                .detach(fragmentMedicines)
                .attach(fragmentMedicines)
                .commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initUI();
        initRealm();
        Utils.createNewUserIfNeeded(HomeActivity.this);

        // USED FOR TESTING THE ALARMS INSERTED
//        Medicine med1 = Utils.getFakeMedicineForTest(this);
//        List<Medicine> list = new ArrayList<>();
//        MedicineDAO dao = new MedicineDAO(realm_instance);
//        Medicine medsave = dao.save(Utils.getUser(this), med1);
//        list.add(medsave);
//        SetAlarmsForMedicineList obj = new SetAlarmsForMedicineList(list, this);
//        obj.setAllAlarms();
    }

    private void initRealm() {
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jt.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded() // todo remove for production
                .build();

        Realm.getInstance(config);

        Realm.setDefaultConfiguration(config);

        realm_instance = Realm.getDefaultInstance();
    }

    private void initUI() {

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        viewPagerBottom = (AHBottomNavigationViewPager) findViewById(R.id.view_pager_bottom);

        //Aqui onde é adicionado os fragments no bottom
        viewPagerBottom.setOffscreenPageLimit(2);
        bottomViewPagerAdapter = new BottomViewPagerAdapter(getSupportFragmentManager());
        bottomViewPagerAdapter.add(fragmentMedicines);
        bottomViewPagerAdapter.add(fragmentNewMedicine);
        bottomViewPagerAdapter.add(fragmentSettings);
        viewPagerBottom.setAdapter(bottomViewPagerAdapter);

        currentFragment = bottomViewPagerAdapter.getCurrentFragment();

        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Remédios", R.drawable.ic_remedio, R.color.colorAccent);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Novo", R.drawable.ic_novo, R.color.colorAccent);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Configurações", R.drawable.ic_configuracao, R.color.colorAccent);

        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);
        bottomNavigationItems.add(item3);

        bottomNavigation.addItems(bottomNavigationItems);

        int colorDarkGreen = ContextCompat.getColor(this, R.color.verdeEscuro);
        int colorDarkBlue = ContextCompat.getColor(this, R.color.azulEscuro);

        bottomNavigation.setAccentColor(colorDarkGreen);
        bottomNavigation.setInactiveColor(colorDarkBlue);
        bottomNavigation.setCurrentItem(0);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                if (currentFragment == null) {
                    currentFragment = bottomViewPagerAdapter.getCurrentFragment();
                }


                if (currentFragment != null) {
                    if (currentFragment instanceof FragmentMedicines) {

                        fragmentMedicines.willBeHidden();

                    } else if (currentFragment instanceof FragmentNewMedicine) {

                        fragmentNewMedicine.willBeHidden();

                    }else if (currentFragment instanceof FragmentSettings) {

                        fragmentSettings.willBeHidden();

                    }
                }

                //Aqui é onde é setado qual o fragment atual
                //Em seguida é pego o fragment atual e feito o fade dependendo de qual instancia for
                viewPagerBottom.setCurrentItem(position, false);
                currentFragment = bottomViewPagerAdapter.getCurrentFragment();

                if (currentFragment instanceof FragmentMedicines) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .detach(currentFragment)
                            .attach(currentFragment)
                            .commit();
                    fragmentMedicines.willBeDisplayed();

                } else if (currentFragment instanceof FragmentNewMedicine) {

                    fragmentNewMedicine.willBeDisplayed();

                }else if (currentFragment instanceof FragmentSettings) {

                    fragmentSettings.willBeDisplayed();

                }

                return true;
            }
        });

        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {
                Log.d("DemoActivity", "BottomNavigation Position: " + y);
            }
        });


    }
}