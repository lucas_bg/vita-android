package br.uel.ihc.vita.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.uel.ihc.vita.R;

/**
 * Classe criada para adaptar a lista de objetos do tipo Medicine para uma ListView.
 * @author Yago Henrique Pereira
 * @since 21/11/2017
 * @version 1.0
 */
public class MedicineAdapter extends ArrayAdapter<Medicine> implements View.OnClickListener {
    private ArrayList<Medicine> dataList;
    private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView medName;
        TextView medDosType;
        TextView medDos;
    }

    public MedicineAdapter(ArrayList<Medicine> data, Context context) {
        super(context, R.layout.z_med_list, data);
        this.dataList = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        Medicine dataModel = (Medicine) object;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Medicine dataModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        MedicineAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new MedicineAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());

            convertView = inflater.inflate(R.layout.z_med_list, parent, false);
            viewHolder.medName = (TextView) convertView.findViewById(R.id.med_title_text);
            viewHolder.medDosType = (TextView) convertView.findViewById(R.id.med_dos_type_text);
            viewHolder.medDos = (TextView) convertView.findViewById(R.id.med_dos_text);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MedicineAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        lastPosition = position;

        viewHolder.medName.setText( dataModel.getTitle() );
        viewHolder.medDosType.setText( dataModel.getDosage_type() );
        viewHolder.medDos.setText( String.valueOf( dataModel.getDosage() ) );

        // Return the completed view to render on screen
        return result;
    }
}
