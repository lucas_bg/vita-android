package br.uel.ihc.vita.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

/**
 * Created by renato on 03/12/17.
 */

public class MedicineAdapter extends ArrayAdapter {

    ArrayList mArrayList;
    Context mContext;

    public MedicineAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public MedicineAdapter(Context context, int resource, ArrayList<HashMap> items, JSONArray jsonArray) {
        super(context, resource, items);
        this.mArrayList = items;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Medicine medicine= (Medicine) mArrayList.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolderMedicine viewHolder = null; // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = new ViewHolderMedicine();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.item_list_view_remedios, parent, false);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.txt_remedio_nome);
            viewHolder.imageViewColor = (ImageView) convertView.findViewById(R.id.img_remedio_etiqueta);
        }

        if (viewHolder != null) {
            viewHolder.textViewName.setText(medicine.getTitle());
            int color = Integer.valueOf(medicine.getColor());
            ((GradientDrawable)viewHolder.imageViewColor.getBackground()).setColor(color);
        }

        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    public static class ViewHolderMedicine {
        public TextView textViewName;
        public ImageView imageViewColor;
    }
}
