package br.uel.ihc.vita.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.uel.ihc.vita.R;

/**
 * Classe criada para adaptar a lista de objetos do tipo User para uma ListView.
 * @author Yago Henrique Pereira
 * @since 14/11/2017
 * @version 1.0
 */
public class UserAdapter extends ArrayAdapter<User> implements View.OnClickListener {
    private ArrayList<User> dataList;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView usName;
        TextView usEmail;
    }

    public UserAdapter(ArrayList<User> data, Context context) {
        super(context, R.layout.z_users_list, data);
        this.dataList = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        User dataModel = (User) object;

        /*switch (v.getId())
        {
            case R.id.item_info:
                System.out.println("");
                break;
        }*/
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        User dataModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.z_users_list, parent, false);
            viewHolder.usName = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.usEmail = (TextView) convertView.findViewById(R.id.user_email);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        lastPosition = position;

        viewHolder.usName.setText(dataModel.getName());
        viewHolder.usEmail.setText(dataModel.getEmail());
        // Return the completed view to render on screen
        return convertView;
    }
}