package br.uel.ihc.vita.alarmsystem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineDose;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by lucas on 04/12/2017
 */

public class SetAlarmsForMedicineList {

    private List<Medicine> medicineList;
    private Context mContext;

    public SetAlarmsForMedicineList(List<Medicine> list, Context ctx) {
        this.medicineList = list;
        this.mContext = ctx;
    }

    public void setAllAlarms() {
        for(Medicine added_med : this.medicineList) {
            //PEGANDO OS DIAS INICIO E FIM E CONVERTENDO PARA MEIA NOITE DESSES DIAS
            Calendar initial_day = Calendar.getInstance();
            Calendar final_day = Calendar.getInstance();
            initial_day.setTime(added_med.getDate_start());
            final_day.setTime(added_med.getDate_end());
//            initial_day.set(initial_day.get(Calendar.YEAR), initial_day.get(Calendar.MONTH),
//                    initial_day.get(Calendar.DAY_OF_MONTH), 0, 0,0);
//            final_day.set(final_day.get(Calendar.YEAR), final_day.get(Calendar.MONTH),
//                    final_day.get(Calendar.DAY_OF_MONTH), 0, 0,0);

            //DECLARANDO A LISTA COM TODAS AS DOSES
            Calendar i_day = Calendar.getInstance();
            //ITERANDO NO PERÍODO ESPECIFICADO
            for (Date date = initial_day.getTime(); initial_day.before(final_day); initial_day.add(Calendar.DATE, 1), date = initial_day.getTime()) {
                i_day.setTime(date);
                String i_day_week_day = i_day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
                //FOR E IF LOGO ABAIXO SÃO PARA FILTRAREM OS DIAS DA SEMANA
                for (String d : added_med.getDays_of_week()) {
                    if (i_day_week_day.equals(d)){
                        //NO PERIODO, HOJE É UM DIA DA SEMANA QUE VAI TER OS ALARMES
                        //GERANDO OS ALARMES DO DIA
                        Calendar c2 = Calendar.getInstance();
                        for (int i = added_med.getStart_time(); i < 24*60; i+=added_med.getInterval()) {
                            if (Utils.isSameDay(i_day, c2)) {
                                int hora = c2.get(Calendar.HOUR_OF_DAY);
                                int minutos = c2.get(Calendar.MINUTE);
                                int x = hora*60 + minutos;
                                if (x > i)
                                    continue;
                            }
                            MedicineDose med_dose = new MedicineDose();
                            med_dose.setMed(added_med);
                            //ADICIONANDO O HORARIO DO DIA NO DIA i_day
                            i_day.set(Calendar.HOUR_OF_DAY, i/60);
                            i_day.set(Calendar.MINUTE, i%60);
                            med_dose.setDiaEHora(i_day.getTime());
                            med_dose.setDose(i);
                            createSpecificAlarm(med_dose);
                        }
                        break;
                    }
                }
            }
        }
    }

    private void createSpecificAlarm(MedicineDose med_dose) {
        AlarmManager a = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);
        PendingIntent p1;
        //Iterando sobre todas as doses do período
        //colocando os Extra para serem usados na tela de lembrete ao usuario
        Intent intent = new Intent(mContext, Alarm.class);
        intent.putExtra("scheduled_time", med_dose.getDiaEHora().getTime());
        intent.putExtra("dose", med_dose.getDose());
        intent.putExtra("dosage", med_dose.getMed().getDosage());
        intent.putExtra("dosage_type", med_dose.getMed().getDosage_type());
        intent.putExtra("med_id", med_dose.getMed().getId());
        intent.putExtra("title", med_dose.getMed().getTitle());
//        Log.d("TESTIING", "---------------------------");
//        Calendar c = Calendar.getInstance();
//        c.setTime(med_dose.getDiaEHora());
//        Log.d("TESTIING", "DATA: " + c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR));
//        Log.d("TESTIING", "scheduled_time: " + med_dose.getDiaEHora().getTime());
//        Log.d("TESTIING", "dose: " + med_dose.getDose());
//        Log.d("TESTIING", "dosage: " + med_dose.getMed().getDosage());
//        Log.d("TESTIING", "dosage_type: " + med_dose.getMed().getDosage_type());
//        Log.d("TESTIING", "med_id: " + med_dose.getMed().getId());
//        Log.d("TESTIING", "title: " + med_dose.getMed().getTitle());
        //criando a pending intent
        int idd = Integer.parseInt(Utils.createAlarmId(med_dose.getDiaEHora().getTime()));
//        Log.d("TESTIING", "idd: " + idd);
        p1 = PendingIntent.getBroadcast(mContext.getApplicationContext(), idd, intent, PendingIntent.FLAG_ONE_SHOT);

        // setando o alarme para o tempo especificado
        if (a != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                a.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, med_dose.getDiaEHora().getTime(), p1);
//                Log.d("TESTIING", "PASSEI AQUI TAMBEM");
            } else {
                a.setExact(AlarmManager.RTC_WAKEUP, med_dose.getDiaEHora().getTime(), p1);
            }
        } else {
            Log.e("ALARMMANAGER", "ERRO AO OBTER ALARMMANAGER");
        }
    }

}
