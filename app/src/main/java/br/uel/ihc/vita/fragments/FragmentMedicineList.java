package br.uel.ihc.vita.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.MedicineDetailActivity;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.adapters.MedicineAdapter;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

public class FragmentMedicineList extends Fragment {

    public FragmentMedicineList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    ListView listView;
    ArrayList list;
    ArrayList listActive;
    ArrayList listInactive;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_medicine_list, container, false);

        listView = (ListView) view.findViewById(R.id.lista);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle bundle = getArguments();
                int index = bundle.getInt("index");

                Intent intent = new Intent(getActivity(), MedicineDetailActivity.class);
                Medicine medicine;
                if (index == 0) {
                    medicine = (Medicine) listActive.get(position);
                } else {
                    medicine = (Medicine) listInactive.get(position);
                }
                intent.putExtra("medicineId", medicine.getId());
                intent.putExtra("index", index);
                startActivity(intent);

            }
        });

        return view;
    }

    public static FragmentMedicineList newInstance(int index, String param, ViewPager viewPager) {
        FragmentMedicineList fragment = new FragmentMedicineList();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        Realm realm_instance = Realm.getDefaultInstance();

        MedicineDAO dao = new MedicineDAO( realm_instance );
        UserDAO daoUser = new UserDAO(realm_instance);

        ArrayList listUser = daoUser.list();
        User user = (User) listUser.get(0);

        list = dao.list(user);


        Bundle bundle = getArguments();
        int index = bundle.getInt("index");

        ArrayList newList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            Medicine medicine = (Medicine)list.get(i);
            if (index == 0) {
                if (medicine.isActive())
                    newList.add(medicine);
            } else {
                if (!medicine.isActive())
                    newList.add(medicine);
            }
        }

        if (index == 0) {
            listActive = new ArrayList();
            listActive = newList;
        } else {
            listInactive = new ArrayList();
            listInactive = newList;
        }
            MedicineAdapter adapter = new MedicineAdapter(getActivity(), 0, newList, null);
        listView.setAdapter(adapter);

    }
}
