package br.uel.ihc.vita;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class ActivitySignUp extends AppCompatActivity {

    private EditText inputName;
    private EditText inputEmail;

    private User userSession;
    private UserDAO daoUser;
    private String mId;

    protected Realm realm_instance;
    View titulo_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sign_up);
        initRealm();

        // get user data
        Realm realm_instance = Realm.getDefaultInstance();
        daoUser = new UserDAO(realm_instance);
        mId = getIntent().getStringExtra("USER_ID");

        userSession = daoUser.getById(mId);

        ImageButton btnBack = findViewById(R.id.sign_up_btn_back);
        Button btnFinish = findViewById(R.id.sign_up_btn_finish);

        inputName = findViewById(R.id.sign_up_input_name);
        inputEmail = findViewById(R.id.sign_up_input_email);

        if(!userSession.getName().isEmpty()){
            inputName.setText( userSession.getName() );
        }

        if(!userSession.getEmail().isEmpty() && !userSession.getEmail().equals("casper@ghost.com")){
            inputEmail.setText( userSession.getEmail() );
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence text;

                if(inputName.getText().toString().isEmpty() || inputEmail.getText().toString().isEmpty()){
                    // Insert in DB
                    text = "Por favor, preencha todos os campos.";
                }else{

                    User upUser = new User();
                    upUser.setName( inputName.getText().toString()  );
                    upUser.setEmail( inputEmail.getText().toString() );
                    upUser.setId( userSession.getId() );

                    userSession = daoUser.update(upUser);

                    if(userSession != null){
                        text = "Seus dados foram gravados!";
                    }else{
                        userSession = daoUser.getById(mId);
                        text = "Houve um problema ao tentar gravar seus dados. Por favor, tente novamente.";
                    }
                }

                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(view.getContext(), text, duration);
                toast.show();

                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        titulo_view = findViewById(R.id.titulo_tela);
        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(ActivitySignUp.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(titulo_view,
                        "Nesta tela você irá preencher o seu nome e " +
                                "e-mail e então irá clicar em \"Terminei!\" para " +
                                "atualizar suas informações de cadastro.", Utils.continuar);

                sequence.start();
            }
        });
    }

    private void initRealm() {
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jt.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded() // todo remove for production
                .build();

        Realm.getInstance(config);

        Realm.setDefaultConfiguration(config);

        realm_instance = Realm.getDefaultInstance();
    }
}