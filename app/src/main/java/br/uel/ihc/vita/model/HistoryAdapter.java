package br.uel.ihc.vita.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.uel.ihc.vita.R;

/**
 * Classe criada para adaptar a lista de objetos do tipo History para uma ListView.
 * @author Yago Henrique Pereira
 * @since 23/11/2017
 * @version 1.0
 */

public class HistoryAdapter extends ArrayAdapter<History> implements View.OnClickListener {
    private ArrayList<History> dataList;
    private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView hisDate;
        TextView hisDosType;
        TextView hisDos;
    }

    public HistoryAdapter(ArrayList<History> data, Context context) {
        super(context, R.layout.z_history_list, data);
        this.dataList = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        History dataModel = (History) object;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        History dataModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        HistoryAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new HistoryAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());

            convertView = inflater.inflate(R.layout.z_history_list, parent, false);
            viewHolder.hisDate = (TextView) convertView.findViewById(R.id.his_date_time_text);
            viewHolder.hisDosType = (TextView) convertView.findViewById(R.id.his_dos_type_text);
            viewHolder.hisDos = (TextView) convertView.findViewById(R.id.his_dos_text);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (HistoryAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        lastPosition = position;

        viewHolder.hisDate.setText( dataModel.getDate_time().toString() );
        viewHolder.hisDosType.setText( dataModel.getDosage_type() );
        viewHolder.hisDos.setText( String.valueOf( dataModel.getDosage() ) );

        // Return the completed view to render on screen
        return result;
    }
}
