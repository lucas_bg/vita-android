package br.uel.ihc.vita.zpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

public class ZUserUpdateActivity extends AppCompatActivity {

    protected Realm realm_instance;
    private Button btn_back;
    private Button btn_update;

    private TextView textName;
    private TextView textEmail;

    private EditText inputName;
    private EditText inputEmail;


    private UserDAO dao;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.z_user_update);

        // Initialize Realm
        Realm.init(this);
        this.realm_instance = Realm.getDefaultInstance();

        // Get User data
        String mId = getIntent().getStringExtra("USER_ID");
        dao = new UserDAO(this.realm_instance);
        mUser = dao.getById(mId);

        inputName = findViewById(R.id.up_input_user_name);
        inputEmail = findViewById(R.id.up_input_user_email);

        // Inform user data
        DisplayInfo();

        //btn_up_page
        btn_update = findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                User up_user = new User();
                up_user.setId( mUser.getId() );
                up_user.setName( inputName.getText().toString() );
                up_user.setEmail( inputEmail.getText().toString() );

                mUser = dao.update(up_user);

                if(mUser != null){
                    DisplayInfo();
                }else{
                    Intent intent = new Intent(getBaseContext(), ZMainActivity.class);
                    startActivity(intent);
                }

                up_user = null;
            }
        });

        // up_btn_back
        btn_back = findViewById(R.id.up_btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(mUser != null && mUser.getId() != null){
                    Intent intent = new Intent(getBaseContext(), ZMedicinesActivity.class);
                    intent.putExtra("USER_ID", mUser.getId());
                    startActivity(intent);
                }else{
                    System.out.println("Houve um erro ao tentar resgatar o ID do Usuário.");
                }
            }
        });
    }

    public void DisplayInfo() {
        textName = findViewById(R.id.up_user_name_text);
        textEmail = findViewById(R.id.up_user_email_text);

        if (mUser != null) {
            textName.setText(   mUser.getName()   );
            textEmail.setText(  mUser.getEmail()  );
        }

        inputName.setText("");
        inputEmail.setText("");
    }
}