package br.uel.ihc.vita.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import br.uel.ihc.vita.ActivitySignUp;
import br.uel.ihc.vita.ActivityTerms;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class FragmentSettings extends Fragment {

    private FrameLayout fragmentContainer;

    private User userSession;
    private View btnTerms;
    private Button btnSignUp;

    private TextView userNullMsg;
    private TextView userNotNullMsg;
    private TextView userNotNullEmail;

    private View status_atual;
    private View termos_txt;
    private View copia_txt;

    protected Realm realm_instance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        // Initiates Realm, User and DAOs
        Realm.init(getContext());
        this.realm_instance = Realm.getDefaultInstance();
        userSession = Utils.getUser(getContext());

        UserDAO daoUser = new UserDAO(realm_instance);

        userNullMsg = (TextView) view.findViewById(R.id.settNullMsg);
        userNotNullMsg = (TextView) view.findViewById(R.id.settNotNullMsg);
        userNotNullEmail = (TextView) view.findViewById(R.id.settNotNullEmail);

        if(userSession == null || userSession.getEmail().isEmpty() || userSession.getEmail().equals("casper@ghost.com")){
            userNullMsg.setVisibility(View.VISIBLE);
            userNotNullMsg.setVisibility(View.GONE);
            userNotNullEmail.setVisibility(View.GONE);
        }else{
            userNullMsg.setVisibility(View.GONE);
            userNotNullMsg.setVisibility(View.VISIBLE);
            userNotNullEmail.setVisibility(View.VISIBLE);
            userNotNullEmail.setText( userSession.getEmail() );
        }

        btnSignUp = (Button) view.findViewById(R.id.settingsBtnSignUp);
        btnTerms = view.findViewById(R.id.settingsTermsBtn);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ActivitySignUp.class);
                intent.putExtra("USER_ID", userSession.getId());
                startActivityForResult(intent, 2);
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ActivityTerms.class);
                intent.putExtra("USER_ID", userSession.getId());
                startActivity(intent);
            }
        });

        copia_txt = view.findViewById(R.id.copia_txt);
        termos_txt = view.findViewById(R.id.termos_uso_txt);
        status_atual = view.findViewById(R.id.status_atual);
        ImageButton btnn = view.findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity());

                sequence.setConfig(config);

                sequence.addSequenceItem(termos_txt,
                        "Aqui você tem acesso aos termos de uso do aplicativo.", Utils.continuar);

                sequence.addSequenceItem(copia_txt,
                        "Aqui é informado o status atual do seu cadastro.", Utils.continuar);

                sequence.addSequenceItem(btnSignUp,
                        "Nesse botão você pode atualizar seu cadastro.", Utils.continuar);

                sequence.start();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .detach(FragmentSettings.this)
                .attach(FragmentSettings.this)
                .commit();
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
            fadeIn.setDuration(1000);
            fragmentContainer.startAnimation(fadeIn);
            fragmentContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
            fadeOut.setStartOffset(1000);
            fadeOut.setDuration(1000);
            fragmentContainer.startAnimation(fadeOut);
        }

    }
}