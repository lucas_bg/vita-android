package br.uel.ihc.vita.DAO;

import java.util.ArrayList;
import java.util.UUID;

import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Classe criada para gerenciar a persistência de dados dos medicamentos.
 * @author Yago Henrique Pereira
 * @since 20/11/2017
 * @version 1.0
 */

public class MedicineDAO {

    private Realm realm_instance;

    /**
      * Método Construtor
      * @param realm - Instância da lib REALM. Esta será utilizada para as funções CRUD.
      */
    public MedicineDAO(Realm realm){
            this.realm_instance = realm;
        }

    /**
      * Método para salvar a instância do objeto no banco de dados REALM.
      * @param user - Instância do usuário. Relação 1 -> n.
      * @param med - Instância que será gravada no banco de dados.
      * @return Retornará o objeto inserido ou o valor null para caso de erro.
      */
    public Medicine save(User user, Medicine med){

        final Medicine db_med;

        //if(
          //      !med.getTitle().isEmpty() && !med.getDosage_type().isEmpty() && med.getDosage() > 0 &&
            //    !med.getDescription().isEmpty() && med.getDate_start() != null && med.getDate_end() != null &&
              //  !med.getColor().isEmpty()
                //){
        if(!med.getTitle().isEmpty() && !med.getDosage_type().isEmpty() && med.getDosage() > 0){
            try{
                this.realm_instance.beginTransaction();

                db_med = this.realm_instance.createObject(Medicine.class, UUID.randomUUID().toString());
                db_med.setUser_id( user.getId() );
                db_med.setTitle( med.getTitle() );
                db_med.setDosage_type( med.getDosage_type() );
                db_med.setDosage( med.getDosage() );
                db_med.setUninterrupted( med.isUninterrupted() );
                db_med.setDescription( med.getDescription() );
                db_med.setDate_start( med.getDate_start() );
                db_med.setDate_end( med.getDate_end() );
                db_med.setColor( med.getColor() );
                db_med.setNotification( med.isNotification() );
                db_med.setActive( med.isActive() );
                db_med.setDays_of_week( med.getDays_of_week() );
                db_med.setInterval( med.getInterval() );
                db_med.setStart_time( med.getStart_time() );

                this.realm_instance.commitTransaction();
            }catch (Exception ex){
                return null;
            }
            return db_med;
        }else{
            return null;
        }
    }

    /**
     * Método para atualizar a instância do objeto no banco de dados REALM.
     * @param med - Instância que será atualizada no banco de dados.
     * @return Retornará o objeto atualizado ou o valor null para caso de erro.
     */
    public Medicine update(Medicine med){

        //if(!med.getTitle().isEmpty() && !med.getDosage_type().isEmpty() && med.getDosage() > 0 &&
          // !med.getDescription().isEmpty() && med.getDate_start() != null && med.getDate_end() != null &&
           //!med.getColor().isEmpty()
           //){
        if(!med.getTitle().isEmpty() && !med.getDosage_type().isEmpty() && med.getDosage() > 0){
            try{
                this.realm_instance.close();
                this.realm_instance.beginTransaction();
                this.realm_instance.copyToRealmOrUpdate( med );
                this.realm_instance.commitTransaction();
            }catch (Exception ex){
                return null;
            }
            return med;
        }else{
            return null;
        }
    }

    /**
      * Método para remover um registro do banco de dados REALM.
      * @param med - Registro que será removido do banco de dados.
      * @return Retornará um valor boolean como confirmação da ação.
      */
    public boolean delete(final Medicine med){
        try{
            this.realm_instance.beginTransaction();
            Medicine med_db = this.realm_instance.where(Medicine.class).equalTo("id", med.getId()).findFirst();
            med_db.deleteFromRealm();
            this.realm_instance.commitTransaction();
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    /**
     * Método criado para a resgatar um único objeto do tipo Medicine registrado na base de dados
     * @param id - Chave primária do objeto que será resgatado.
     * @return Retornará uma instância de um objeto do tipo Medicine ou o valor null, em caso de erros.
     */
    public Medicine getById(String id){
        Medicine result;
        try{
            this.realm_instance.beginTransaction();
            Medicine med_db = this.realm_instance.where(Medicine.class).equalTo("id", id).findFirst();
            result = this.realm_instance.copyFromRealm( med_db );
            this.realm_instance.commitTransaction();
        }catch (Exception e){
            result = null;
        }
        return result;
    }

    /**
     * Método criado para a listagem de todos os objetos do tipo Medicine, que respeitam a condição
     * da chave estrangeira informada como parâmetro.
     * @param user Instância do usuário com o id para listagem
     * @return Retornará uma lista com objetos do tipo Medicine.
     */
    public ArrayList<Medicine> list(User user){

        ArrayList<Medicine> resultMeds = new ArrayList();

        this.realm_instance.beginTransaction();

        RealmResults<Medicine> query = this.realm_instance.where(Medicine.class)
                .equalTo("user_id", user.getId())
                .findAllSorted("date_start", Sort.DESCENDING);

        if(!query.isEmpty() && query.size() > 0){
            resultMeds.addAll( query );
        }

        this.realm_instance.commitTransaction();

        return resultMeds;
    }
}
