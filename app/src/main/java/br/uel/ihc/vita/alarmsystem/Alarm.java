package br.uel.ihc.vita.alarmsystem;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import java.util.List;

import br.uel.ihc.vita.DAO.HistoryDAO;
import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lucas on 28/11/2017
 */

public class Alarm extends BroadcastReceiver {

    //WHAT TO DO WHEN THE TIME TO ALERT COMES
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager.WakeLock wakeLock = null;
        PowerManager pm;
        pm = ((PowerManager)context.getSystemService(Context.POWER_SERVICE));
        if (pm != null)
            wakeLock = pm.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "MyActivity");
        if (wakeLock != null)
            wakeLock.acquire(2000);

        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jt.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded() // todo remove for production
                .build();
        Realm.getInstance(config);
        Realm.setDefaultConfiguration(config);
        // Verificando por usuário
        Utils.createNewUserIfNeeded(context);

        String med_id = intent.getStringExtra("med_id");
        int dose = intent.getIntExtra("dose", 0);

        Realm.init(context);
        Realm realm_instance = Realm.getDefaultInstance();
        User mUser = Utils.getUser(context);
        HistoryDAO hist_dao = new HistoryDAO(realm_instance);
        List<History> list = hist_dao.listToday(mUser);
        for (History h : list) {
            if (h.getMed_id().equals(med_id) && h.getScheduled_time() == dose)
                return;
        }
        MedicineDAO medicineDAO = new MedicineDAO(realm_instance);
        Medicine med = medicineDAO.getById(intent.getStringExtra("med_id"));
        if (!med.isActive())
            return;

        Intent newIntent = new Intent(context, DisplayActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        newIntent.putExtra("scheduled_time",
                intent.getLongExtra("scheduled_time", 0));
        newIntent.putExtra("dosage", intent.getIntExtra("dosage", 0));
        newIntent.putExtra("dose", intent.getIntExtra("dose", 0));
        newIntent.putExtra("dosage_type", intent.getStringExtra("dosage_type"));
        newIntent.putExtra("med_id", intent.getStringExtra("med_id"));
        newIntent.putExtra("title", intent.getStringExtra("title"));
        context.startActivity(newIntent);
        if (wakeLock != null)
            wakeLock.release();
    }

}
