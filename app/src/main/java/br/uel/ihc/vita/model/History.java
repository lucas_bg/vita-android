package br.uel.ihc.vita.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Classe criada para gerar objetos do tipo History.
 * @author Yago Henrique Pereira
 * @since 23/11/17
 * @version 1.0
 */

public class History extends RealmObject {
    @PrimaryKey
    private String id;

    private String user_id;
    private String med_id;

    private Date date_time;
    private String dosage_type;
    private int dosage;
    private boolean took;
    private int scheduled_time;

    public int getScheduled_time() {
        return scheduled_time;
    }

    public void setScheduled_time(int scheduled_time) {
        this.scheduled_time = scheduled_time;
    }

    public History(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMed_id() {
        return med_id;
    }

    public void setMed_id(String med_id) {
        this.med_id = med_id;
    }

    public Date getDate_time() {
        return date_time;
    }

    public void setDate_time(Date date_time) {
        this.date_time = date_time;
    }

    public String getDosage_type() {
        return dosage_type;
    }

    public void setDosage_type(String dosage_type) {
        this.dosage_type = dosage_type;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public boolean isTook() {
        return took;
    }

    public void setTook(boolean took) {
        this.took = took;
    }
}
