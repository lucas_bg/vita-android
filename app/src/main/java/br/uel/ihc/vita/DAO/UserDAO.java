package br.uel.ihc.vita.DAO;

import java.util.ArrayList;
import java.util.UUID;

import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Classe criada para gerenciar a persistência de dados dos usuários.
 * @author Yago Henrique Pereira
 * @since 20/11/2017
 * @version 1.0
 */

public class UserDAO {

    private Realm realm_instance;

    /**
     * Método Construtor
     * @param realm - Instância da lib REALM. Esta será utilizada para as funções CRUD.
     */
    public UserDAO(Realm realm){
        this.realm_instance = realm;
    }

    /**
     * Método para salvar a instância do objeto no banco de dados REALM.
     * @param user - Instância que será gravada no banco de dados.
     * @return Retornará o objeto inserido ou o valor null para caso de erro.
     */
    public User save(User user){

        User db_user = null;

        if(!user.getEmail().isEmpty() && !user.getName().isEmpty()){
            try{
                this.realm_instance.beginTransaction();
                db_user = this.realm_instance.createObject(User.class, UUID.randomUUID().toString());
                db_user.setName( user.getName() );
                db_user.setEmail( user.getEmail() );
                this.realm_instance.commitTransaction();
            }catch (Exception ex){
                return null;
            }

            return db_user;
        }else{
            return null;
        }
    }

    /**
     * Método para atualizar a instância do objeto no banco de dados REALM.
     * @param user - Instância que será atualizada no banco de dados.
     * @return Retornará o objeto atualizado ou o valor null para caso de erro.
     */
    public User update(User user){

        if(!user.getEmail().isEmpty() && !user.getName().isEmpty()){
            try{
                this.realm_instance.close();

                this.realm_instance.beginTransaction();

                this.realm_instance.copyToRealmOrUpdate(user);

                this.realm_instance.commitTransaction();
            }catch (Exception ex){
                return null;
            }

            return user;
        }else{
            return null;
        }
    }

    /**
     * Método para remover um registro do banco de dados REALM.
     * @param user - Registro que será removido do banco de dados.
     * @return Retornará um valor boolean como confirmação da ação.
     */
    public boolean delete(final User user){
        try{
            this.realm_instance.beginTransaction();

            // Primeiro remove todos os remédios desse usuário
            RealmResults<Medicine> result = this.realm_instance.where(Medicine.class)
                    .equalTo("user_id", user.getId())
                    .findAll();

            result.deleteAllFromRealm();

            // Após remover os remédios, remove o usuário
            User user_db = this.realm_instance.where(User.class).equalTo("id", user.getId()).findFirst();
            user_db.deleteFromRealm();

            this.realm_instance.commitTransaction();
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    /**
     * Método criado para a resgatar um único objeto do tipo User registrado na base de dados
     * @param id - Chave primária do objeto que será resgatado.
     * @return Retornará uma instância de um objeto do tipo User ou o valor null, em caso de erros.
     */
    public User getById(String id){
        User result;
        try{
            this.realm_instance.beginTransaction();
            result = this.realm_instance.where(User.class).equalTo("id", id).findFirst();
            this.realm_instance.commitTransaction();
        }catch (Exception e){
            result = null;
        }
        return result;
    }

    /**
     * Método criado para a listagem de todos os objetos do tipo User registrados na base de dados
     * @return Retornará uma lista com objetos do tipo User.
     */
    public ArrayList<User> list(){
        ArrayList<User> user_list = new ArrayList<User>();

        this.realm_instance.beginTransaction();
        RealmQuery<User> query = this.realm_instance.where(User.class);

        if(query.count() > 0){
            user_list = new ArrayList( query.findAll() );
        }else{
            user_list = new ArrayList();
        }

        this.realm_instance.commitTransaction();
        return user_list;
    }
}
