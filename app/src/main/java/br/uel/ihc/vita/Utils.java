package br.uel.ihc.vita;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineDose;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by renato on 26/11/17.
 */

public class Utils {

    public final static String continuar = "Ok! Clique aqui para continuar.";

    public static void buildAlertWithText (Activity context, String message, String buttonText) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.tela_alert_um_botao, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        Button button = dialogView.findViewById(R.id.buttonOk);
        button.setText(buttonText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        TextView textViewMessage = (TextView)dialogView.findViewById(R.id.tvMessage);
        textViewMessage.setText(message);

    }



    public static void buildList(Activity activity, ArrayList<String> arrayList, int message, final EditText editTextToUse) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setTitle(message);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(activity, android.R.layout.select_dialog_singlechoice);
        for (String string : arrayList) {
            arrayAdapter.add(string);
        }

        builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                editTextToUse.setText(strName);
                dialog.dismiss();
            }
        });
        builderSingle.show();
    }

    public static User getUser(Context context) {
        Realm.init(context);
        Realm realm_instance = Realm.getDefaultInstance();
        User mUser = new User();
        UserDAO dao = new UserDAO(realm_instance);

        List<User> list = dao.list();
        if (list.isEmpty()) {
            mUser.setName("Usuario");
            mUser.setEmail("usuario@usuario.com");
            dao.save(mUser);
        } else {
            mUser = list.get(0);
        }

        return mUser;
    }

    public static String fromIntMinutesToStringHour(int minutes) {
        int hour = minutes/60;
        int minute = minutes%60;
        String result;
        if (minute < 10) {
            result = String.valueOf(hour) + "h0" + String.valueOf(minute);
        } else {
            result = String.valueOf(hour) + "h" + String.valueOf(minute);
        }
        return result;
    }

    public static Medicine getFakeMedicineForTest(Context context){
        Medicine med = new Medicine();

        med.setDescription("esse é um remedio");
        med.setActive(true);
        med.setColor("-2818048");
        med.setDate_start(new Date());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
        med.setDate_end(new Date(cal.getTimeInMillis()));
        RealmList<String> list = new RealmList<>();
        Calendar calendar = Calendar.getInstance();
        list.add(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US));
        med.setDays_of_week(list);
        med.setDosage(5);
        med.setDosage_type("gotas");
        med.setInterval(60);
        med.setStart_time(360);
        med.setTitle("Remedio X");
        med.setUser_id(Utils.getUser(context).getId());
        med.setUninterrupted(false);
        med.setNotification(true);

        return med;
    }

    public static MedicineDose getFakeMedicineDoseForTest() {
        MedicineDose med_dose = new MedicineDose();
        Medicine med = new Medicine();
        med_dose.setDose(1325);
        med.setTitle("ZOmeprazol");
        med.setDosage_type("Zgotas");
        med.setDosage(20);
        med.setDescription("ZTomar no desjejum");
        med_dose.setMed(med);
        return med_dose;
    }

    public static void createNewUserIfNeeded (Context context) {
        User us = new User("Casper", "casper@ghost.com");

        Realm realm_instance = Realm.getDefaultInstance();
        UserDAO dao = new UserDAO( realm_instance );

        ArrayList arrayList = dao.list();

        if (arrayList.size() == 0) {
            CharSequence text;
            dao = new UserDAO(realm_instance);
            dao.save(us);
//            if (result != null) {
//                text = "Registro inserido com sucesso!";
//            } else {
//                text = "Houve um problema na inserção.";
//            }
//
//            Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//            toast.show();

        }
    }

    public static String createAlarmId(long param) {
        Date now = new Date();
        now.setTime(param);
        return new SimpleDateFormat("YYMMddHHmm",  Locale.US).format(now);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

}
