package br.uel.ihc.vita;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.model.Medicine;
import io.realm.Realm;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.shape.RectangleShape;

public class MedicineDetailActivity extends AppCompatActivity {

    ImageButton btn_voltar;
    TextView txt_remedio_nome;
    TextView txt_remedio_nome2;
    TextView txt_notificacao;
    ImageView imageViewColor;
    Button btn_interromper_uso;
    Button btn_excluir;
    Medicine med;
    TextView txt_dosage_and_dosage_type;
    TextView txt_usoContinuo;
    TextView txt_dataInicio;
    TextView txt_dataTermino;
    TextView txt_horario;
    TextView txt_diadasemana;
    TextView txt_observacoes;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_informacoesremedio_dois);


        btn_voltar = findViewById(R.id.btn_voltar);
        txt_remedio_nome = findViewById(R.id.titulo_tela);
        txt_remedio_nome2 = findViewById(R.id.txt_remedio_nome2);
        txt_notificacao = findViewById(R.id.txt_notificacao);
        imageViewColor = findViewById(R.id.imageViewColor);
        btn_interromper_uso = findViewById(R.id.btn_interromper_uso);
        btn_excluir = findViewById(R.id.btn_excluir);
        txt_dosage_and_dosage_type = findViewById(R.id.txt_dosage_and_dosage_type);
        txt_usoContinuo = findViewById(R.id.txt_usoContinuo);
        txt_dataInicio = findViewById(R.id.txt_dataInicio);
        txt_dataTermino = findViewById(R.id.txt_dataTermino);
        txt_horario = findViewById(R.id.txt_horario);
        txt_diadasemana = findViewById(R.id.txt_diadasemana);
        txt_observacoes = findViewById(R.id.txt_observacoes);

        String idMed = getIntent().getStringExtra("medicineId");

        Realm realm_instance = Realm.getDefaultInstance();

        MedicineDAO dao = new MedicineDAO( realm_instance );

        med = dao.getById(idMed);

        if (med.isUninterrupted())
            txt_usoContinuo.setText("Sim");
        else
            txt_usoContinuo.setText("Não");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(med.getDate_start());
        String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String ano = String.valueOf(calendar.get(Calendar.YEAR));
        txt_dataInicio.setText(dia + "/" + mes + "/" + ano);
        calendar.setTime(med.getDate_end());
        dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        ano = String.valueOf(calendar.get(Calendar.YEAR));
        txt_dataTermino.setText(dia + "/" + mes + "/" + ano);
        txt_horario.setText(Utils.fromIntMinutesToStringHour(med.getStart_time()));
        txt_observacoes.setText(med.getDescription());
        List<String> lista = new ArrayList<>();
        for (String s : med.getDays_of_week()) {
            lista.add(getDiaDaSemanaEmPt(s));
        }
        StringBuilder dias_semana = new StringBuilder();
        for (String s : lista) {
            dias_semana.append(s).append(", ");
        }
        dias_semana = new StringBuilder(dias_semana.substring(0, dias_semana.length() - 2));
        txt_diadasemana.setText(dias_semana.toString());

        txt_remedio_nome.setText("Detalhes");
        txt_remedio_nome2.setText(med.getTitle());
        int color = Integer.valueOf(med.getColor());
        ((GradientDrawable)imageViewColor.getBackground()).setColor(color);

        setDosage();

        boolean isNotification = med.isNotification();
        if (isNotification)
            txt_notificacao.setText("Sim");
        else
            txt_notificacao.setText("Não");

        if (!med.isActive()) {
            btn_interromper_uso.setText("Atualizar e Ativar");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btn_interromper_uso.setBackgroundTintList(ColorStateList.valueOf(R.color.verdeEscuro));
            }
        }

        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_interromper_uso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (med.isActive()) {
                    createDialog(R.string.confirm_interrupt,false);
                } else {
                    updateStatusMedicine(true);
                }

            }
        });

        btn_excluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog(R.string.confirm_delete, true);
            }
        });

        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view
                config.setShapePadding(0);
                config.setShape(new RectangleShape(txt_remedio_nome.getWidth(), txt_remedio_nome.getHeight()));

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(MedicineDetailActivity.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(txt_remedio_nome,
                        "Nesta tela você pode ver todas as informações " +
                                "sobre o remédio específico que você clicou." +
                                "No botão Interromper Uso você pode interromper o uso de um " +
                                "medicamento e ele será marcado como inativo. " +
                                "No botão Atualizar e Ativar você pode ativar o remédio e voltar " +
                                "a tomá-lo regularmente. No botão Excluir o " +
                                "remédio será excluído permanentemente.", Utils.continuar);

                sequence.start();
            }
        });
    }

    private void  setDosage () {
        if (med.getDosage() > 1) {
            if (med.getDosage_type().equals("cápsula") || med.getDosage_type().equals("ampola")) {
                txt_dosage_and_dosage_type.setText(String.valueOf(med.getDosage()) + " " + med.getDosage_type() + "s");
            }
        } else {
            txt_dosage_and_dosage_type.setText(String.valueOf(med.getDosage()) + " " + med.getDosage_type());
        }
    }

    private void createDialog (int text, final boolean isDelete) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MedicineDetailActivity.this);
        LayoutInflater inflater = MedicineDetailActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.tela_alert_dois_botoes, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        Button button = dialogView.findViewById(R.id.btn_sim);
        button.setText(R.string.yes);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDelete) {
                    deleteMedicine();
                } else {
                    updateStatusMedicine(false);
                }
                alertDialog.dismiss();
                finish();
            }
        });

        Button buttonTwo = dialogView.findViewById(R.id.btn_nao);
        buttonTwo.setText(R.string.no);
        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        TextView textViewMessage = dialogView.findViewById(R.id.mensagem_txt_view);
        textViewMessage.setText(text);

    }

    private void deleteMedicine() {
        Realm realm_instance = Realm.getDefaultInstance();
        MedicineDAO medicineDAO = new MedicineDAO(realm_instance);
        medicineDAO.delete(med);
        finish();
    }

    private void updateStatusMedicine(boolean isActive) {
        Realm realm_instance = Realm.getDefaultInstance();
        MedicineDAO medicineDAO = new MedicineDAO(realm_instance);
        med.setActive(isActive);
        medicineDAO.update(med);
        finish();
    }

    private String getDiaDaSemanaEmPt(String value) {
        switch (value) {
            case "Sunday":
                return "Domingo";
            case "Monday":
                return "Segunda-feira";
            case "Tuesday":
                return "Terça-feira";
            case "Wednesday":
                return "Quarta-feira";
            case "Thursday":
                return "Quinta-feira";
            case "Friday":
                return "Sexta-feira";
            case "Saturday":
                return "Sábado";
        }
        return "";
    }
}
