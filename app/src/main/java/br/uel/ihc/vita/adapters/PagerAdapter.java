package br.uel.ihc.vita.adapters;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import br.uel.ihc.vita.fragments.FragmentMedicineList;

/**
 * Created by renato on 04/12/17.
 */

public class PagerAdapter extends FragmentPagerAdapter{

    private static int NUM_ITEMS = 2;
    public static int currentIndex;
    private static ViewPager mViewPager;

    public PagerAdapter(FragmentManager fm, ViewPager viewPager) {
        super(fm);
        mViewPager = viewPager;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentMedicineList.newInstance(0, "Ativos", mViewPager);
            case 1:
                return FragmentMedicineList.newInstance(1, "Inativos", mViewPager);
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Ativos";
        return "Inativos";
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
