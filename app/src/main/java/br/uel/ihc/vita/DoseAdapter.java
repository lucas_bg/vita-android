package br.uel.ihc.vita;

import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineDose;

/**
 * Created by lucas on 06/12/2017
 */

public class DoseAdapter extends ArrayAdapter<MedicineDose> implements View.OnClickListener {

    private List<MedicineDose> dataList;
    private Context mContext;

    @Override
    public void onClick(View view) {

    }

    // View lookup cache
    private static class ViewHolder {
        ImageView doseCor;
        TextView doseTexto;
        ImageView doseTomouOuNao;
    }

    public DoseAdapter(List<MedicineDose> data, Context context) {
        super(context, R.layout.today_doses_item, data);
        this.dataList = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        MedicineDose dataModel = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        DoseAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new DoseAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());

            convertView = inflater.inflate(R.layout.today_doses_item, parent, false);
            viewHolder.doseCor = convertView.findViewById(R.id.cor_medicine_dose);
            viewHolder.doseTexto = convertView.findViewById(R.id.texto_medicine_dose);
            viewHolder.doseTomouOuNao = convertView.findViewById(R.id.tomou_ou_nao_medicine_dose);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DoseAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        if (dataModel != null){
            Medicine med = dataModel.getMed();
            String htmlString = "<b>" + dataModel.getMed().getTitle() + "</b><br>"
                    + String.valueOf(med.getDosage()) + " " + med.getDosage_type() +
                    " - " + Utils.fromIntMinutesToStringHour(dataModel.getDose());
            viewHolder.doseTexto.setText(Html.fromHtml(htmlString));

            int color = Integer.valueOf(med.getColor());
            ((GradientDrawable)viewHolder.doseCor.getBackground()).setColor(color);
            if (dataModel.isTomou() == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.branco), android.graphics.PorterDuff.Mode.SRC_IN);
                } else {
                    viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.branco), android.graphics.PorterDuff.Mode.SRC_IN);
                }
            } else {
                if (dataModel.isTomou()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewHolder.doseTomouOuNao.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_sim, mContext.getApplicationContext().getTheme()));
                        viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.verdeEscuro), android.graphics.PorterDuff.Mode.SRC_IN);
                    } else {
                        viewHolder.doseTomouOuNao.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_sim));
                        viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.verdeEscuro), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        viewHolder.doseTomouOuNao.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_nao, mContext.getApplicationContext().getTheme()));
                        viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.vermelhoEscuro), android.graphics.PorterDuff.Mode.SRC_IN);
                    } else {
                        viewHolder.doseTomouOuNao.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_nao));
                        viewHolder.doseTomouOuNao.setColorFilter(ContextCompat.getColor(mContext, R.color.vermelhoEscuro), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                }
            }
        }

        return result;
    }

}
