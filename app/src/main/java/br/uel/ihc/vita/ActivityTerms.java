package br.uel.ihc.vita;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class ActivityTerms extends AppCompatActivity {

    View titulo_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terms);

        ImageButton btnBack = findViewById(R.id.terms_btn_back);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        titulo_view = findViewById(R.id.titulo_tela);
        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(ActivityTerms.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(titulo_view,
                        "Nesta tela é possível ver quais são os termos de uso do" +
                                " app que você está usando neste momento.", Utils.continuar);

                sequence.start();
            }
        });
    }
}