package br.uel.ihc.vita.zpackage;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.MotionEvent;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.model.User;
import br.uel.ihc.vita.model.UserAdapter;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.text.InputType;
import android.view.View.OnClickListener;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ZMainActivity extends AppCompatActivity implements ZTab_Ativos.OnFragmentInteractionListener , ZTab_Inativos.OnFragmentInteractionListener, OnClickListener
{
    //UI References
    private EditText et_remedio_dt_inicio;
    private EditText et_remedio_dt_termino;

    private DatePickerDialog dt_inicioDatePickerDialog;
    private DatePickerDialog dt_terminoDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    protected Realm realm_instance;


    private ArrayList<User> user_list;
    private ListView listView;
    private UserAdapter adapter;
    private Button btn_save;

    //
    private UserDAO dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int view_atual = R.layout.z_principal;
        setContentView(view_atual);

        //inicializar tela de cadastro parte 1
        if(view_atual ==  R.layout.z_cadastro_remedio_parte1) {
            dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

            findViewsById();

            setDateTimeField();

            Realm.init(this);

            RealmConfiguration config = new RealmConfiguration.Builder()
                    .name("jt.realm")
                    .schemaVersion(1)
                    .deleteRealmIfMigrationNeeded() // todo remove for production
                    .build();

            Realm.getInstance(config);

            Realm.setDefaultConfiguration(config);

            realm_instance = Realm.getDefaultInstance();

            findUsers();

            btn_save = findViewById(R.id.btn_save);
            btn_save.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    EditText input_name = findViewById(R.id.user_name);
                    EditText input_email = findViewById(R.id.user_email);

                    User us_insert = new User( input_name.getText().toString(),
                            input_email.getText().toString());

                    saveUser(us_insert);
                }
            });

        }

        //inicializar tela LISTA DE REMEDIOS (ativos/inativos)
        if(view_atual ==  R.layout.z_lista_remedios) {
            TabLayout tabLayout = findViewById(R.id.tablayout);
            tabLayout.addTab(tabLayout.newTab().setText("Ativos"));
            tabLayout.addTab(tabLayout.newTab().setText("Inativos"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            final ViewPager viewPager = findViewById(R.id.pager);

            final ZPagerAdapter adapter = new ZPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }

            });

            viewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });

            viewPager.setOnPageChangeListener(new SimpleOnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    viewPager.getParent().requestDisallowInterceptTouchEvent(true);
                }
            });
        }

    }

    private void findViewsById() {
        et_remedio_dt_inicio = findViewById(R.id.input_remedio_data_inicio);
        et_remedio_dt_inicio.setInputType(InputType.TYPE_NULL);
        et_remedio_dt_inicio.requestFocus();

        et_remedio_dt_termino = findViewById(R.id.input_remedio_data_termino);
        et_remedio_dt_termino.setInputType(InputType.TYPE_NULL);
    }

    private void setDateTimeField() {
        et_remedio_dt_inicio.setOnClickListener(this);
        et_remedio_dt_termino.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        dt_inicioDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_remedio_dt_inicio.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        dt_terminoDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_remedio_dt_termino.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View view) {
        if(view == et_remedio_dt_inicio) {
            dt_inicioDatePickerDialog.show();
        } else if(view == et_remedio_dt_termino) {
            dt_terminoDatePickerDialog.show();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        setContentView(R.layout.tela_termodeuso);

    }

    public void findUsers(){

        dao = new UserDAO( realm_instance );
        user_list = dao.list();

        listView = findViewById(R.id.sampleListView);
        adapter = new UserAdapter(user_list, getApplicationContext());
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(view.getContext(), ZMedicinesActivity.class);
                intent.putExtra("USER_ID", user_list.get(position).getId());
                startActivity(intent);
            }
        });
    }

    public void saveUser(User us) {

        CharSequence text;
        int duration = Toast.LENGTH_SHORT;

        dao = new UserDAO( realm_instance );
        User result = dao.save( us );
        if( result != null ){
            user_list.add( result );
            adapter.notifyDataSetChanged();
            text = "Registro inserido com sucesso!";
        }else{
            text = "Houve um problema na inserção.";
        }

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();

    }
}
