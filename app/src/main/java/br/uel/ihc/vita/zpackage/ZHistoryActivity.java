package br.uel.ihc.vita.zpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import br.uel.ihc.vita.DAO.HistoryDAO;
import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.DAO.UserDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.HistoryAdapter;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineAdapter;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

public class ZHistoryActivity extends AppCompatActivity {
    protected Realm realm_instance;

    private ArrayList<History> his_list;
    private ArrayList<Medicine> med_list;
    private ListView med_listView;
    private MedicineAdapter med_adapter;

    private ListView his_listView;
    private HistoryAdapter his_adapter;

    private Button his_btn_back;
    private Button his_btn_del;

    private UserDAO dao;
    private User mUser;

    private MedicineDAO med_dao;
    private HistoryDAO his_dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.z_history);

        // Initialize Realm
        Realm.init(this);
        this.realm_instance = Realm.getDefaultInstance();

        // Get User data
        String mId = getIntent().getStringExtra("USER_ID");
        dao = new UserDAO(this.realm_instance);
        mUser = dao.getById(mId);

        his_btn_del = findViewById(R.id.his_btn_del);
        his_btn_del.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                deleteAllHistory();
                findHistory();
            }
        });

        // set buttons //////////////////////////////////////////////////////////
        his_btn_back = findViewById(R.id.his_btn_back);
        his_btn_back.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ZMedicinesActivity.class);
                intent.putExtra("USER_ID", mUser.getId());
                startActivity(intent);
            }
        });

        findMedicines();
        findHistory();
    }

    public void findHistory(){

        his_dao = new HistoryDAO( this.realm_instance );
        his_list = his_dao.list( mUser );

        his_listView = findViewById(R.id.hisListView);
        his_adapter = new HistoryAdapter(his_list, getApplicationContext());
        his_adapter.notifyDataSetChanged();
        his_listView.setAdapter(his_adapter);

        his_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dt = his_list.get(position).getDate_time().toString();

                if(his_dao.delete( his_list.get(position) )){

                    his_list.remove( position );
                    his_adapter.notifyDataSetChanged();

                    CharSequence text = "O Remédio das " + dt + " foi removido com sucesso!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(view.getContext(), text, duration);
                    toast.show();
                }else{
                    System.out.println("Houve um problema na deleção do registro.");
                }
            }
        });
    }

    public void deleteAllHistory(){
        CharSequence text;
        if(his_dao.deleteAll(mUser)){
            text = "Histórico excluído com sucesso!";
        }else{
            text = "Houve um problema com a deleção. Por favor, tente novamente.";
        }
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getBaseContext(), text, duration);
        toast.show();
    }

    public void findMedicines(){

        med_dao = new MedicineDAO( this.realm_instance );
        med_list = med_dao.list( mUser );

        med_listView = findViewById(R.id.his_medListView);
        med_adapter = new MedicineAdapter(med_list, getApplicationContext());
        med_adapter.notifyDataSetChanged();
        med_listView.setAdapter(med_adapter);

        med_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = med_list.get(position).getTitle();

                History his_db = new History();
                his_db.setUser_id( mUser.getId() );
                his_db.setMed_id( med_list.get(position).getId() );
                his_db.setDosage_type( med_list.get(position).getDosage_type() );
                his_db.setDosage( med_list.get(position).getDosage() );
                his_db.setDate_time( new Date() );

                CharSequence text;
                if(his_dao.save( his_db ) != null){
                    text = "Adicionao ao histórico!";
                    findHistory();
                }else{
                    text = "Houve um erro ao tentar salvar no histórico";
                }

                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(view.getContext(), text, duration);
                toast.show();
            }
        });
    }
}