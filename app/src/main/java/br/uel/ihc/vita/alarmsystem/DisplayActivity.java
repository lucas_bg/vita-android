package br.uel.ihc.vita.alarmsystem;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import br.uel.ihc.vita.DAO.HistoryDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;

/**
 * Created by lucas on 28/11/2017
 */

public class DisplayActivity extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        Window wind = this.getWindow();
        wind.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        wind.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        wind.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_display);
        Intent intent = getIntent();

        TextView message = findViewById(R.id.txt_remedio_message);
        Button btn_tomar = findViewById(R.id.btn_tomar);
        Button btn_ignorar = findViewById(R.id.btn_ignorar);
        Button btn_desligar_som = findViewById(R.id.btn_desligar_som);

        Calendar calendar = Calendar.getInstance();
        final String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        final String mes = String.valueOf(calendar.get(Calendar.MONTH));
        final String ano = String.valueOf(calendar.get(Calendar.YEAR));
        final String hora = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        final String minuto = String.valueOf(calendar.get(Calendar.MINUTE));
        String date = dia + "/" + mes + "/" + ano + " as " + hora + ":" + minuto;
        int dosage = intent.getIntExtra("dosage", 0);
        String dosage_type = intent.getStringExtra("dosage_type");
        String med_name = intent.getStringExtra("title");

        message.setText("É hora de tomar " + dosage + " " +
                dosage_type + " do remédio " + med_name +
        " programado para " + date);

        // VOLUME TO MAXIMUM
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int max_volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, max_volume,
                    AudioManager.FLAG_VIBRATE);
        }

        // INITIATES STANDARD ALARM SOUND
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, notification);
        mediaPlayer.start();

        Realm.init(this);
        Realm realm_instance = Realm.getDefaultInstance();
        User mUser = Utils.getUser(this);
        final HistoryDAO hist_dao = new HistoryDAO(realm_instance);
        final History history = new History();
        history.setMed_id(intent.getStringExtra("med_id"));
        history.setDosage_type(dosage_type);
        history.setDosage(dosage);
        history.setDate_time(new Date());
        history.setScheduled_time(intent.getIntExtra("dose", 0));
        history.setUser_id(mUser.getId());

        btn_tomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                history.setTook(true);
                hist_dao.save(history);
                mediaPlayer.release();
                finish();
            }
        });

        btn_ignorar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                history.setTook(false);
                hist_dao.save(history);
                mediaPlayer.release();
                finish();
            }
        });

        btn_desligar_som.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.release();
                finish();
            }
        });

    }

}
