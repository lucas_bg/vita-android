package br.uel.ihc.vita;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.uel.ihc.vita.DAO.HistoryDAO;
import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineDose;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class TodayMedicineActivity extends AppCompatActivity {

    private List<Medicine> med_list;
    private double percentage;
    private ImageView coracao;
    View titulo_view;

    @Override
    protected void onResume() {
        super.onResume();
        doStuff();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doStuff();
    }

    private void doStuff() {
        setContentView(R.layout.tela_remedio_hoje_new);

        TextView data_hoje_txt_view = findViewById(R.id.dia_semana_e_mes);
        Calendar calendar = Calendar.getInstance();
        String dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String ano = String.valueOf(calendar.get(Calendar.YEAR));
        String dia_semana = getDiaDaSemanaEmPt(calendar.get(Calendar.DAY_OF_WEEK));
        data_hoje_txt_view.setText(dia_semana + ", " + dia + "/" + mes + "/" + ano);

        // Initiates Realm, User and DAOs
        Realm.init(this);
        Realm realm_instance = Realm.getDefaultInstance();
        User mUser = Utils.getUser(this);
        MedicineDAO med_dao = new MedicineDAO(realm_instance);
        HistoryDAO hist_dao = new HistoryDAO(realm_instance);

        //Get all medicines
        med_list = med_dao.list(mUser);
        //Get medicines for today
        List<Medicine> today_med_list = getMedicinesForToday();
        //Get z_history for today
        final List<History> hist_list = hist_dao.listToday(mUser);
        //Get all the doses for today
        List<MedicineDose> med_dose_list = new ArrayList<>();
        for (Medicine med : today_med_list) {
            for (int i = med.getStart_time(); i < 1440; i+=med.getInterval()) {
                MedicineDose med_dose = new MedicineDose();
                med_dose.setMed(med);
                med_dose.setDose(i);
                if (med.isActive())
                    med_dose_list.add(med_dose);
            }
        }

        List<MedicineDose> ja_tomados_list = new ArrayList<>();
        for (History h : hist_list) {
            for (MedicineDose md : med_dose_list) {
                if(h.getScheduled_time() == md.getDose() && h.getMed_id().equals(md.getMed().getId())) {
                    med_dose_list.remove(md);
                    md.setTomou(h.isTook());
                    ja_tomados_list.add(md);
                    break;
                }
            }
        }
        for (MedicineDose md : med_dose_list) {
            md.setTomou(null);
        }
        for (MedicineDose md : ja_tomados_list) {
            Collections.addAll(med_dose_list, md);
        }
        Collections.sort(med_dose_list);

        calculatePercentageForHeart(med_dose_list);

        chooseImageAndTextForHeart();

        final ListView listView = findViewById(R.id.doses_list_view);
        DoseAdapter adapter = new DoseAdapter(med_dose_list, getApplicationContext());
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MedicineDose med_dose = (MedicineDose) listView.getItemAtPosition(position);
                Intent intent = new Intent(TodayMedicineActivity.this, MedicineDetailActivity.class);
                intent.putExtra("medicineId", med_dose.getMed().getId());
                startActivity(intent);
            }
        });

        ImageButton btn_voltar = findViewById(R.id.btn_voltar);
        btn_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        titulo_view = findViewById(R.id.titulo_tela);
        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(TodayMedicineActivity.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(titulo_view,
                        "Nesta tela você pode ver todas as doses " +
                                "dos seus remédios para hoje.", Utils.continuar);

                sequence.addSequenceItem(coracao,
                        "Nesse coração é mostrado se você está tomando " +
                                "os seus remédios no horário! Caso atrase o coração " +
                                "esvazia um pouco e se você tomar o remédio o coração " +
                                "se enche novamente! Por isso tome seus " +
                                "remédios no horário!!", Utils.continuar);

                sequence.start();
            }
        });
    }

    private void chooseImageAndTextForHeart() {
        coracao = findViewById(R.id.coracao);
        TextView textoDoCoracao = findViewById(R.id.textoDoCoracao);
        Drawable drawableImage;
        int v = (int) Math.round(percentage * 10);

        switch (v) {
            case 0:
                drawableImage = getResources().getDrawable(R.drawable.heart0);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você não tomou nenhum remedio!");
                return;
            case 1:
                drawableImage = getResources().getDrawable(R.drawable.heart10);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou poucos remédios!");
                return;
            case 2:
                drawableImage = getResources().getDrawable(R.drawable.heart20);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou poucos remédios!");
                return;
            case 3:
                drawableImage = getResources().getDrawable(R.drawable.heart30);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou poucos remédios!");
                return;
            case 4:
                drawableImage = getResources().getDrawable(R.drawable.heart40);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou poucos remédios!");
                return;
            case 5:
                drawableImage = getResources().getDrawable(R.drawable.heart50);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou apenas metade dos remédios para o horário!");
                return;
            case 6:
                drawableImage = getResources().getDrawable(R.drawable.heart60);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou apenas pouco mais da metade dos remédios para o horário!");
                return;
            case 7:
                drawableImage = getResources().getDrawable(R.drawable.heart70);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você tomou apenas pouco mais da metade dos remédios para o horário!");
                return;
            case 8:
                drawableImage = getResources().getDrawable(R.drawable.heart80);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você ainda não tomou alguns remédios mas está tomando a maioria!");
                return;
            case 9:
                drawableImage = getResources().getDrawable(R.drawable.heart90);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Você ainda não tomou alguns remédios mas está tomando a maioria!");
                return;
            case 10:
                drawableImage = getResources().getDrawable(R.drawable.heart100);
                coracao.setImageDrawable(drawableImage);
                textoDoCoracao.setText("Parabéns, você está tomando os remédios no horário!");
                return;
            default:
                return;
        }
    }

    private void calculatePercentageForHeart(List<MedicineDose> med_dose_list) {
        Calendar calendarr = Calendar.getInstance();
        int hora = calendarr.get(Calendar.HOUR_OF_DAY);
        int minutos = calendarr.get(Calendar.MINUTE);
        int valor_agora = hora*60 + minutos;

        int doses_antes_agora=0, tomados=0;
        for (MedicineDose md : med_dose_list) {
            if (md.getDose() <= valor_agora)
                doses_antes_agora++;
            if (md.isTomou()!=null && md.isTomou())
                tomados++;
        }

        this.percentage = (double) tomados / (double) doses_antes_agora;
        if (percentage > 1)
            percentage = 1.0;
    }

    private List<Medicine> getMedicinesForToday() {
        Date curr_date = new Date();
        List<Medicine> today_med_listt = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        String day_oftw_of_today = calendar.getDisplayName(
                Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        for (Medicine med : med_list) {
            if(curr_date.after(med.getDate_start()) && curr_date.before(med.getDate_end())) {
                for (String day_oftw : med.getDays_of_week()) {
                    if (day_oftw.equals(day_oftw_of_today)) {
                        today_med_listt.add(med);
                        break;
                    }
                }
            }
        }
        return today_med_listt;
    }

    private String getDiaDaSemanaEmPt(int value) {
        switch (value) {
            case Calendar.SUNDAY:
                return "Domingo";
            case Calendar.MONDAY:
                return "Segunda-feira";
            case Calendar.TUESDAY:
                return "Terça-feira";
            case Calendar.WEDNESDAY:
                return "Quarta-feira";
            case Calendar.THURSDAY:
                return "Quinta-feira";
            case Calendar.FRIDAY:
                return "Sexta-feira";
            case Calendar.SATURDAY:
                return "Sábado";
        }
        return "";
    }

}
