package br.uel.ihc.vita;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class NewRegisterActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, CheckBox.OnCheckedChangeListener{

    EditText etMeasureUnit;
    EditText etContinousUse;
    EditText etStartDate;
    EditText etFinalDate;
    EditText etStartTime;
    int startTimeValue = 0;
    EditText etIntervalTime;
    EditText etCurrent;
    EditText etMedicineName;
    EditText etMedicineQtd;

    CheckBox checkBox1;
    CheckBox checkBox2;
    CheckBox checkBox3;
    CheckBox checkBox4;
    CheckBox checkBox5;
    CheckBox checkBox6;
    CheckBox checkBox7;

    ArrayList arrayListUnit;
    ArrayList arrayListContinuosUse;
    ArrayList arrayListWeekdays;

    boolean isChecked;

    View titulo_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_register);

        ImageButton buttonBack = (ImageButton) findViewById(R.id.btn_voltar);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button buttonRegister = (Button)findViewById(R.id.btn_proximo);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkFields()) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("name", etMedicineName.getText().toString());
                    hashMap.put("qtd", etMedicineQtd.getText().toString());
                    hashMap.put("measure", etMeasureUnit.getText().toString());
                    hashMap.put("isContinous", etContinousUse.getText());
                    hashMap.put("startDate", etStartDate.getText().toString());
                    hashMap.put("finalDate", etFinalDate.getText().toString());
                    hashMap.put("startTime", String.valueOf(startTimeValue));
                    int intervaltime = Integer.valueOf(etIntervalTime.getText().toString());
                    hashMap.put("intervalTime", String.valueOf(intervaltime*60));

                    Intent intent = new Intent(NewRegisterActivity.this, NewRegisterTwoActivity.class);
                    intent.putExtra("hash", hashMap);
                    intent.putExtra("weekdays", arrayListWeekdays);
                    startActivity(intent);
                }
            }
        });

        arrayListUnit = new ArrayList();
        arrayListUnit.add("mg");
        arrayListUnit.add("cápsula");
        arrayListUnit.add("ml");
        arrayListUnit.add("ampola");

        arrayListContinuosUse = new ArrayList();
        arrayListContinuosUse.add("Sim");
        arrayListContinuosUse.add("Não");

        arrayListWeekdays = new ArrayList();

        etMedicineName = (EditText)findViewById(R.id.input_remedio_nome);
        etMedicineQtd = (EditText)findViewById(R.id.input_remedio_quantidade);

        checkBox1 = (CheckBox)findViewById(R.id.cb_domingo);
        checkBox2 = (CheckBox)findViewById(R.id.cb_segunda);
        checkBox3 = (CheckBox)findViewById(R.id.cb_terca);
        checkBox4 = (CheckBox)findViewById(R.id.cb_quarta);
        checkBox5 = (CheckBox)findViewById(R.id.cb_quinta);
        checkBox6 = (CheckBox)findViewById(R.id.cb_sexta);
        checkBox7 = (CheckBox)findViewById(R.id.cb_sabado);
        checkBox1.setOnCheckedChangeListener(this);
        checkBox2.setOnCheckedChangeListener(this);
        checkBox3.setOnCheckedChangeListener(this);
        checkBox4.setOnCheckedChangeListener(this);
        checkBox5.setOnCheckedChangeListener(this);
        checkBox6.setOnCheckedChangeListener(this);
        checkBox7.setOnCheckedChangeListener(this);

        etMeasureUnit = (EditText)findViewById(R.id.input_remedio_unidade_medida);
        etMeasureUnit.setKeyListener(null);
        etMeasureUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.buildList(NewRegisterActivity.this, arrayListUnit, R.string.select_measure_unit, etMeasureUnit);
            }
        });

        etContinousUse = (EditText)findViewById(R.id.et_remedio_unidade_medida);
        etContinousUse.setKeyListener(null);
        etContinousUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFinalDate.setText("");
                Utils.buildList(NewRegisterActivity.this, arrayListContinuosUse, R.string.continous_use, etContinousUse);
            }
        });

        final Calendar calendar = Calendar.getInstance();
        final int startYear = calendar.get(Calendar.YEAR);
        final int starthMonth = calendar.get(Calendar.MONTH);
        final int startDay = calendar.get(Calendar.DAY_OF_MONTH);

        etStartDate = (EditText)findViewById(R.id.input_remedio_data_inicio);
        etStartDate.setKeyListener(null);
        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCurrent = etStartDate;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        NewRegisterActivity.this, NewRegisterActivity.this, startYear, starthMonth, startDay);
                datePickerDialog.show();
            }
        });

        etFinalDate = (EditText)findViewById(R.id.input_remedio_data_termino);
        etFinalDate.setKeyListener(null);
        etFinalDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etContinousUse.getText().toString().equals("Sim")) {
                    Toast.makeText(NewRegisterActivity.this, R.string.warning_continuous_use, Toast.LENGTH_LONG).show();
                    return;
                }
                etCurrent = etFinalDate;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        NewRegisterActivity.this, NewRegisterActivity.this, startYear, starthMonth, startDay);
                datePickerDialog.show();
            }
        });

        etStartTime = (EditText)findViewById(R.id.input_remedio_horario_inicio);
        etStartTime.setKeyListener(null);
        etStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(NewRegisterActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        etStartTime.setText( selectedHour + ":" + selectedMinute);
                        startTimeValue = selectedHour * 60 + selectedMinute;
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        etIntervalTime = (EditText)findViewById(R.id.input_remedio_intervalo);
        etIntervalTime.setKeyListener(null);
        etIntervalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final NumberPicker picker = new NumberPicker(NewRegisterActivity.this);
                picker.setMinValue(1);
                picker.setMaxValue(23);

                FrameLayout layout = new FrameLayout(NewRegisterActivity.this);
                layout.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

                new AlertDialog.Builder(NewRegisterActivity.this)
                        .setView(layout)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String value = String.valueOf(picker.getValue());
                                etIntervalTime.setText(value);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();

            }
        });

        titulo_view = findViewById(R.id.titulo_tela);
        ImageButton btnn = findViewById(R.id.btn_duvida);
        btnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(500); // half second between each showcase view

                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(NewRegisterActivity.this);

                sequence.setConfig(config);

                sequence.addSequenceItem(titulo_view,
                        "Nesta tela você irá preencher algumas informações " +
                                "sobre o seu medicamento e ao final irá clicar no botão " +
                                "\"Próximo Passo\" para continuar.", Utils.continuar);

                sequence.start();
            }
        });

    }

    public boolean checkFields () {

        if (
                (etMedicineName.length() == 0 ||
                etMedicineQtd.length() == 0 ||
                etMeasureUnit.length() == 0 ||
                etContinousUse.length() == 0 ||
                etStartDate.length() == 0 ||
                etStartTime.length() == 0 || startTimeValue == 0 ||
                etIntervalTime.length() == 0 ||
                        !isChecked
                ) || (etContinousUse.getText().toString().equals("Não") && etFinalDate.length() == 0))

        {
            Toast.makeText(this, "Preencha todos os campos.", Toast.LENGTH_LONG).show();
            return false;
        }

        String dataa_inicio = etStartDate.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dateInicio = format.parse(dataa_inicio);
            Calendar escolhida_usuario = Calendar.getInstance();
            escolhida_usuario.setTime(dateInicio);
            Calendar today = Calendar.getInstance();
            if (dateInicio.before(new Date())) {
                if (escolhida_usuario.get(Calendar.DAY_OF_YEAR) != today.get(Calendar.DAY_OF_YEAR)) {
                    Toast.makeText(this, "A data de início não pode ser antes de hoje!", Toast.LENGTH_LONG).show();
                    return false;
                }
                if (escolhida_usuario.get(Calendar.DAY_OF_YEAR) != today.get(Calendar.DAY_OF_YEAR) &&
                        escolhida_usuario.get(Calendar.YEAR) != today.get(Calendar.YEAR) ) {
                    Toast.makeText(this, "A data de início não pode ser antes de hoje!", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        } catch (ParseException e) {e.printStackTrace();}

        return true;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        etCurrent.setText(String.format("%02d", i2) + "/" + String.format("%02d", i1 + 1) + "/" + i);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        isChecked =true;
        if (b) {
            arrayListWeekdays.add(compoundButton.getTag());
        } else {
            arrayListWeekdays.remove(compoundButton.getTag());
        }
    }

}
