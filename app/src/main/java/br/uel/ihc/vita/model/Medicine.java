package br.uel.ihc.vita.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Classe criada para gerar objetos do tipo Medicine.
 * @author Yago Henrique Pereira
 * @since 13/11/2017
 * @version 1.0
 */

public class Medicine extends RealmObject {

    @PrimaryKey
    private String id;

    private String user_id;

    private String title;
    private String dosage_type;
    private int dosage;
    private boolean uninterrupted;
    private String description;
    private Date date_start;
    private Date date_end;
    private String color;
    private boolean notification;
    private boolean active;
    private int start_time;
    private RealmList<String> days_of_week;
    private int interval;

    public Medicine(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDosage_type() {
        return dosage_type;
    }

    public void setDosage_type(String dosage_type) {
        this.dosage_type = dosage_type;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public boolean isUninterrupted() {
        return uninterrupted;
    }

    public void setUninterrupted(boolean uninterrupted) {
        this.uninterrupted = uninterrupted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getDate_end() {
        return date_end;
    }

    public void setDate_end(Date date_end) {
        this.date_end = date_end;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isNotification() {
        return notification;
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getStart_time() {
        return start_time;
    }

    public void setStart_time(int start_time) {
        this.start_time = start_time;
    }

    public RealmList<String> getDays_of_week() {
        return days_of_week;
    }

    public void setDays_of_week(RealmList<String> days_of_week) {
        this.days_of_week = days_of_week;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
