package br.uel.ihc.vita.fragments;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.uel.ihc.vita.DAO.HistoryDAO;
import br.uel.ihc.vita.DAO.MedicineDAO;
import br.uel.ihc.vita.R;
import br.uel.ihc.vita.TodayMedicineActivity;
import br.uel.ihc.vita.Utils;
import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.Medicine;
import br.uel.ihc.vita.model.MedicineDose;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class FragmentMedicines extends Fragment {

    private FrameLayout fragmentContainer;

    protected Realm realm_instance;
    private ArrayList<Medicine> med_list;
    private HistoryDAO hist_dao;
    private History new_history;
    private ImageButton btn_sim_tomei;
    private ImageButton btn_nao_tomei;
    private View go_todays_medicines;
    private RelativeLayout nextMedicineView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Initiates Realm, User and DAOs
        Realm.init(getContext());
        this.realm_instance = Realm.getDefaultInstance();
        User mUser = Utils.getUser(getContext());
        MedicineDAO med_dao = new MedicineDAO(this.realm_instance);
        hist_dao = new HistoryDAO(this.realm_instance);

        //Get all medicines
        med_list = med_dao.list(mUser);
        //Get medicines for today
        List<Medicine> today_med_list = getMedicinesForToday();
        //Get history for today
        final List<History> hist_list = hist_dao.listToday(mUser);
        //Get all the doses for today
        List<MedicineDose> med_dose_list = new ArrayList<>();
        for (Medicine med : today_med_list) {
            for (int i = med.getStart_time(); i < 1440; i+=med.getInterval()) {
                MedicineDose med_dose = new MedicineDose();
                med_dose.setMed(med);
                med_dose.setDose(i);
                if (med.isActive())
                    med_dose_list.add(med_dose);
            }
        }
        //Get the next medicine to take by taking all the doses from today and comparing
        //which ones are not yet in the today history
        for (History h : hist_list) {
            for (MedicineDose md : med_dose_list) {
                if(h.getScheduled_time() == md.getDose() && h.getMed_id().equals(md.getMed().getId())) {
                    med_dose_list.remove(md);
                    break;
                }
            }
        }
        //Orderna a lista de doses e pega a primeira
        //se vazia, acabou (vai pro parabens)
        Collections.sort(med_dose_list);
        MedicineDose nextMedicine;
        if (!med_dose_list.isEmpty())
            nextMedicine = med_dose_list.get(0);
        else
            nextMedicine = null;
        //Decides by congratz screen or netxMedicine
        View view;
        if (nextMedicine == null) {
            // infla a tela de parabens
            view = inflater.inflate(R.layout.parabens, container, false);
            fragmentContainer = view.findViewById(R.id.fragment_container_congratz);
            go_todays_medicines = view.findViewById(R.id.buttonok);
            go_todays_medicines.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), TodayMedicineActivity.class);
                    startActivity(intent);
                }
            });
            ImageButton btnn = view.findViewById(R.id.btn_duvida);
            btnn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowcaseConfig config = new ShowcaseConfig();
                    config.setDelay(500); // half second between each showcase view

                    MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity());

                    sequence.setConfig(config);

                    sequence.addSequenceItem(go_todays_medicines,
                            "Em \"Ver remédios de hoje\" você pode visualizar todas as " +
                                    "suas doses de hoje!", Utils.continuar);

                    sequence.start();
                }
            });
        } else {
            // coloca a tela com o remedio e os botoes
            view = inflater.inflate(R.layout.fragment_medicines, container, false);
            fragmentContainer = view.findViewById(R.id.fragment_container_1);
            // botao para o TodayMedicine
            go_todays_medicines = view.findViewById(R.id.buttonok);
            go_todays_medicines.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), TodayMedicineActivity.class);
                    startActivity(intent);
                }
            });
            // Definicoes do layout
            TextView txt_med_title = view.findViewById(R.id.txt_remedio_nome);
            TextView txt_med_dose = view.findViewById(R.id.txt_remedio_horario);
            TextView txt_med_dosage = view.findViewById(R.id.txt_remedio_quantidade);
            TextView txt_med_dosage_type = view.findViewById(R.id.txt_remedio_unidade_medida);
            TextView txt_med_description = view.findViewById(R.id.txt_remedio_observacao);
            ImageView cor_med = view.findViewById(R.id.img_med_cor);
            int color = Integer.valueOf(nextMedicine.getMed().getColor());
            ((GradientDrawable)cor_med.getBackground()).setColor(color);
            txt_med_title.setText(nextMedicine.getMed().getTitle());
            txt_med_dosage.setText(String.valueOf(nextMedicine.getMed().getDosage()));
            txt_med_dosage_type.setText(nextMedicine.getMed().getDosage_type());
            txt_med_description.setText(nextMedicine.getMed().getDescription());
            txt_med_dose.setText(Utils.fromIntMinutesToStringHour(nextMedicine.getDose()));
            btn_sim_tomei = view.findViewById(R.id.ibtn_tomou_ic_sim);
            btn_nao_tomei = view.findViewById(R.id.ibtn_tomou_ic_nao);
            ImageButton btnn = view.findViewById(R.id.btn_duvida);
            nextMedicineView = view.findViewById(R.id.layout_campo1);
            btnn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowcaseConfig config = new ShowcaseConfig();
                    config.setDelay(500); // half second between each showcase view

                    MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity());

                    sequence.setConfig(config);

                    sequence.addSequenceItem(nextMedicineView,
                            "Aqui ficam todas as informações " +
                                    "da sua próxima dose a ser tomada.", Utils.continuar);

                    sequence.addSequenceItem(btn_sim_tomei,
                            "Quando você tiver tomado o remédio acima, clique neste botão.", Utils.continuar);

                    sequence.addSequenceItem(btn_nao_tomei,
                            "Se você por acaso se esqueceu e não tomou a dose desse " +
                                    "horário do remédio acima, clique neste botão.", Utils.continuar);

                    sequence.addSequenceItem(go_todays_medicines,
                            "Em \"Ver remédios de hoje\" você pode visualizar todas as " +
                                    "suas doses de hoje!", Utils.continuar);

                    sequence.start();
                }
            });
            // Criacao do objeto History
            new_history = new History();
            new_history.setUser_id(mUser.getId());
            new_history.setScheduled_time(nextMedicine.getDose());
            new_history.setDate_time(new Date());
            new_history.setDosage(nextMedicine.getMed().getDosage());
            new_history.setDosage_type(nextMedicine.getMed().getDosage_type());
            new_history.setMed_id(nextMedicine.getMed().getId());
            // Botao sim
            btn_sim_tomei.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Codigo para o Dialog
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.tela_alert_dois_botoes, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    TextView textViewMessage = dialogView.findViewById(R.id.mensagem_txt_view);
                    textViewMessage.setText("Deseja mesmo tomar o remedio?");

                    Button btn_sim = dialogView.findViewById(R.id.btn_sim);
                    Button btn_nao = dialogView.findViewById(R.id.btn_nao);
                    btn_sim.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Caso confirme, salva History e atualiza UI
                            new_history.setTook(true);
                            hist_dao.save(new_history);
                            alertDialog.dismiss();
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .detach(FragmentMedicines.this)
                                    .attach(FragmentMedicines.this)
                                    .commit();
                        }
                    });
                    btn_nao.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Caso nao, apenas volta
                            alertDialog.dismiss();
                        }
                    });
                }
            });
            // Botao nao
            btn_nao_tomei.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Exibe a Dialog
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.tela_alert_dois_botoes, null);
                    dialogBuilder.setView(dialogView);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    TextView textViewMessage = dialogView.findViewById(R.id.mensagem_txt_view);
                    textViewMessage.setText("Deseja mesmo marcar o remedio como não tomado?");

                    Button btn_sim = dialogView.findViewById(R.id.btn_sim);
                    Button btn_nao = dialogView.findViewById(R.id.btn_nao);
                    btn_sim.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Caso confirme, salva History e atualiza UI
                            new_history.setTook(false);
                            hist_dao.save(new_history);
                            alertDialog.dismiss();
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .detach(FragmentMedicines.this)
                                    .attach(FragmentMedicines.this)
                                    .commit();
                        }
                    });
                    btn_nao.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Caso nao, apenas volta
                            alertDialog.dismiss();
                        }
                    });
                }
            });
        }

        return view;
    }

    private List<Medicine> getMedicinesForToday() {
        Date curr_date = new Date();
        List<Medicine> today_med_listt = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        String day_oftw_of_today = calendar.getDisplayName(
                Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);


        for (Medicine med : med_list) {
            if(curr_date.after(med.getDate_start()) && curr_date.before(med.getDate_end())) {
                for (String day_oftw : med.getDays_of_week()) {
                    if (day_oftw.equals(day_oftw_of_today)) {
                        today_med_listt.add(med);
                        break;
                    }
                }
            }
        }
        return today_med_listt;
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
            fadeIn.setDuration(1000);
            fragmentContainer.startAnimation(fadeIn);
            fragmentContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
            fadeOut.setStartOffset(1000);
            fadeOut.setDuration(1000);
            fragmentContainer.startAnimation(fadeOut);
        }

    }
}
