package br.uel.ihc.vita.DAO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import br.uel.ihc.vita.model.History;
import br.uel.ihc.vita.model.User;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Classe criada para gerenciar a persistência de dados dos medicamentos.
 * @author Yago Henrique Pereira
 * @since 23/11/2017
 * @version 1.0
 */

public class HistoryDAO {
    private Realm realm_instance;

    /**
     * Método Construtor
     * @param realm - Instância da lib REALM. Esta será utilizada para as funções CRUD.
     */
    public HistoryDAO(Realm realm){
        this.realm_instance = realm;
    }

    /**
     * Método para salvar a instância do objeto no banco de dados REALM.
     * @param history - Instância do que será guardada no banco de dados.
     * @return Retornará o objeto inserido ou o valor null para caso de erro.
     */
    public History save(History history){

        final History db_his;

        if(
              !history.getUser_id().isEmpty() && !history.getMed_id().isEmpty() && history.getDosage() > 0 &&
            !history.getDosage_type().isEmpty() && history.getDate_time() != null
        ){

            try{
                this.realm_instance.beginTransaction();

                db_his = this.realm_instance.createObject(History.class, UUID.randomUUID().toString());
                db_his.setUser_id(      history.getUser_id()       );
                db_his.setMed_id(       history.getMed_id()        );
                db_his.setDosage_type(  history.getDosage_type()   );
                db_his.setDosage(       history.getDosage()        );
                db_his.setDate_time(    history.getDate_time()     );
                db_his.setTook(         history.isTook()           );
                db_his.setScheduled_time( history.getScheduled_time() );

                this.realm_instance.commitTransaction();
            }catch (Exception ex){
                return null;
            }
            return db_his;
        }else{
            return null;
        }
    }

    /**
     * Método para remover um registro do banco de dados REALM.
     * @param history - Registro que será removido do banco de dados.
     * @return Retornará um valor boolean como confirmação da ação.
     */
    public boolean delete(final History history){
        try{
            this.realm_instance.beginTransaction();
            History his_db = this.realm_instance.where(History.class).equalTo("id", history.getId()).findFirst();
            his_db.deleteFromRealm();
            this.realm_instance.commitTransaction();
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    /**
     * Método para remover um registro do banco de dados REALM.
     * @param user - Instância do usuário com o id, para a exclusão da lista
     * @return Retornará um valor boolean como confirmação da ação.
     */
    public boolean deleteAll(final User user){
        try{
            this.realm_instance.beginTransaction();

            RealmResults<History> result = this.realm_instance.where(History.class)
                    .equalTo("user_id", user.getId())
                    .findAll();

            result.deleteAllFromRealm();

            this.realm_instance.commitTransaction();
        }catch(Exception ex){
            return false;
        }
        return true;
    }

    /**
     * Método criado para a resgatar um único objeto do tipo History registrado na base de dados
     * @param id - Chave primária do objeto que será resgatado.
     * @return Retornará uma instância de um objeto do tipo History ou o valor null, em caso de erros.
     */
    public History getById(String id){
        History result;
        try{
            this.realm_instance.beginTransaction();
            History his_db = this.realm_instance.where(History.class).equalTo("id", id).findFirst();
            result = this.realm_instance.copyFromRealm( his_db );
            this.realm_instance.commitTransaction();
        }catch (Exception e){
            result = null;
        }
        return result;
    }

    /**
     * Método criado para a listagem de todos os objetos do tipo History, que respeitam a condição
     * da chave estrangeira informada como parâmetro.
     * @param user Instância do usuário com o id para listagem
     * @return Retornará uma lista com objetos do tipo History.
     */
    public ArrayList<History> list(User user){

        ArrayList<History> resultHis = new ArrayList();

        this.realm_instance.beginTransaction();

        RealmResults<History> query = this.realm_instance.where(History.class)
                .equalTo("user_id", user.getId())
                .findAllSorted("date_time", Sort.DESCENDING);

        if(!query.isEmpty() && query.size() > 0){
            resultHis.addAll( query );
        }

        this.realm_instance.commitTransaction();

        return resultHis;
    }

    /**
     * Método criado para a listagem de todos os objetos do tipo History, que respeitam a condição
     * da chave estrangeira informada como parâmetro e correspondem à data atual.
     * @param user Instância do usuário com o id para listagem
     * @return Retornará uma lista com objetos do tipo History.
     */
    public ArrayList<History> listToday(User user){

        ArrayList<History> resultHis = new ArrayList();

        Date dt1 = new Date();
        Date dt2 = new Date();

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.set(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH), 0, 0,0);
        c2.set(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH), 23, 59, 59);

        dt1.setTime(c1.getTimeInMillis());
        dt2.setTime(c2.getTimeInMillis());

        this.realm_instance.beginTransaction();

        RealmResults<History> query = this.realm_instance.where(History.class)
                .equalTo("user_id", user.getId())
                .between("date_time", dt1, dt2)
                .findAllSorted("date_time", Sort.DESCENDING);

        if(!query.isEmpty() && query.size() > 0){
            resultHis.addAll( query );
        }

        this.realm_instance.commitTransaction();

        return resultHis;
    }
}
